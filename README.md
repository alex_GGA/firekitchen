---

# Proyecto final Fire-Kitchen

Proyecto final realizado para la materia de Seminario de Ingeniería de Software.

**Integrantes:**

- GAYTAN GUTIERREZ ALEJANDRO
- LIZAOLA GARCIA ALMA DENISSE
- MARTINEZ CRUZ CESAR OSVALDO
- RAMOS TINTA CHRISTIAN EDUARDO
- OROZCO RODRIGUEZ KEVIN ADRIAN

------

## ¿Como ejecutar el programa utilizando el ".jar"?

Para que este proyecto pueda funcionar de manera correcta se deben de seguir los siguientes pasos:

1. Clonar el repositorio.
2. Tener instalado **XAMPP**.
3. Crear una base de datos dentro de **phpmyadmin** llamada "restaurante".
4. Importar dentro de esa base de datos el archivo que se encuentra en la carpeta **Base de Datos/restaurante.sql**.
5. Iniciar el módulo **Apache** y **Mysql** en **XAMPP**.
6. Ejecutar el programa que se encuentra almacenado dentro de la carpeta **src/dist/Fire_Kitchen.jar**.

------

## ¿Como ejecutar el programa desde el IDE de Netbeans?

Para que este proyecto pueda funcionar de manera correcta se denen de seguir los siguientes pasos:

1. Clonar el repositorio.
2. Tener instalado **XAMPP**.
3. Crear una base de datos dentro de **phpmyadmin** llamada "restaurante".
4. Importar dentro de esa base de datos el archivo que se encuentra en la carpeta **Base de Datos/restaurante.sql**.
5. Iniciar el módulo **Apache** y **Mysql** en **XAMPP**.
6. Guardar dentro del proyecto en la carpeta **Libraries** los dos ".jar" que se encuentran almacenados dentro de la carpeta "Librerias de JAVA" y son los siguientes:
	* RSScaleLabel.jar → Sirve para escalar las imagenes dentro de los JLabel.
	* mysql-connector-java-5.1.41-bin.jar → Sirve para realizar la conexión a la base de datos.
7. Ejecutar el proyecto.

-------

## Problemas que podrian ocurrir

* Si aparece el siguiente error al momento de ahcer login "com.mysql.jdbc.exceptions.jdbc4.CommunicationsException: Communications link failure" → Su solución es encender los modulos de **Apache** y **Mysql** en **XAMPP**.
* Si aparece el siguiente error al momento de ahcer login "com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException: Unknown database "restaurante" → Su solución es crear la base de datos deentro de **phpmyadmin** llamada "restaurante" y despues importar el archivo que se encuentra en la dirección **Base de Datos/restaurante.sql**.

-------

## Nombre de usuarios y contraseña para iniciar sesión en el programa

#### Gerente

* Usuario: 214578587
* Contraseña: benja19

#### Mesero

* Usuario: alex_19
* Contraseña: password
