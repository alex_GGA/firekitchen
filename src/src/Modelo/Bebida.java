/*
    Este archivo contiene los datos necesarios para crear el modelo "Bebida":
    Sus atributos son los siguientes:

    [x] Id
    [x] nombre
    [x] precio
    [x] categoria
    [x] foto
    [x] usuario id
    
    [x] Contiene sus constructores para mandarlo a llamar desde otras clases.
    [x] contiene sus setters y getters.
    
*/
package Modelo;

public class Bebida {
    private int id;
    private String nombre;
    private String precio;
    private String categoria;
    byte[] foto;
    private int usuario_id;

    /*
        Constructores
    */
    
    public Bebida() {
    }

    public Bebida(int id, String nombre, String precio, String categoria, byte[] foto, int usuario_id) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.categoria = categoria;
        this.foto = foto;
        this.usuario_id = usuario_id;
    }

    public Bebida(int id, String nombre, String precio, String categoria, int usuario_id) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.categoria = categoria;
        this.usuario_id = usuario_id;
    }

    public Bebida(String nombre, String precio, String categoria, byte[] foto, int usuario_id) {
        this.nombre = nombre;
        this.precio = precio;
        this.categoria = categoria;
        this.foto = foto;
        this.usuario_id = usuario_id;
    }

    public Bebida(int id) {
        this.id = id;
    }

    public Bebida(String nombre) {
        this.nombre = nombre;
    }
    
    /*
        Setters y Getters
    */
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }
}
