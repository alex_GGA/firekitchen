/*
    Este clase se encarga de realizar todos los querys que necesitamos directamente a la base de datos y contiene las siguientes funciones.

    [x] Obtener todos los platillos y almacenarlos en su combo box.
    [x] Obtener todas las bebidas y almacenarlos en su combo box.
    [x] Encontrar un platillo en especifico en la base de datos.
    [x] Encontrar una bebida en especifica en la base de datos.
    [x] Finalizar una orden.
    [x] Obtener todos los platillo s que se han realizado de una mesa en especifico en la base de datos.
    [x] Obtener el id de una orden en la base de datos.
    [x] Agregar platillos y bebidas comprados.
*/
package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;


public class MesaDAO {
    // Variables que necesitamos:
    
    Conexion cn = new Conexion(); // Mandamos a llamar a nuestra clase que realiza la conexión a la base de datos.
    Connection con;               // Variable para ejecutar la conexión  
    PreparedStatement ps;         // Variable para guardar el query y ejecutarlo.
    ResultSet rs;                 // Variable para almacenar los resultados.
    
    public ArrayList<Platillo> PlatilloComboBox(){
        ArrayList<Platillo> platillos = new ArrayList();
        String sql = "SELECT * FROM platillos;";
        try {
            con = cn.MySQLConnect();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
               Platillo platillo = new Platillo(rs.getString("nombre"));
               platillos.add(platillo);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }
        return platillos;
    }
    
    public Platillo PlatilloBuscado(String nombre){
        String sql = "SELECT * FROM platillos WHERE nombre = ?;";
        Platillo platillo = new Platillo();
        try {
            con = cn.MySQLConnect();
            ps = con.prepareStatement(sql);
            ps.setString(1,nombre);
            rs = ps.executeQuery();
            if(rs.next()){
                if(rs.getString("nombre").equals(nombre)){
                    platillo = new Platillo(rs.getInt("id"),
                                                rs.getString("nombre"),
                                                rs.getString("precio"),
                                                rs.getString("categoria"),
                                                rs.getBytes("imagen"),
                                                rs.getInt("usuario_id") 
                    );   
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }
        return platillo;
    }
    
    public ArrayList<Bebida> BebidaComboBox(){
        ArrayList<Bebida> bebidas = new ArrayList();
        String sql = "SELECT * FROM bebidas;";
        try {
            con = cn.MySQLConnect();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                Bebida bebida = new Bebida(rs.getString("nombre"));
                bebidas.add(bebida);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }
        return bebidas;
    }
    
    public Bebida BebidaBuscado(String nombre){
        String sql = "SELECT * FROM bebidas WHERE nombre = ?;";
        Bebida bebida = new Bebida();
        try {
            con = cn.MySQLConnect();
            ps = con.prepareStatement(sql);
            ps.setString(1,nombre);
            rs = ps.executeQuery();
            if(rs.next()){
                if(rs.getString("nombre").equals(nombre)){
                    bebida = new Bebida(rs.getInt("id"),
                                                rs.getString("nombre"),
                                                rs.getString("precio"),
                                                rs.getString("categoria"),
                                                rs.getBytes("imagen"),
                                                rs.getInt("usuario_id")     
                );
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }
        return bebida;
    }

    public void GuardarPedido(ArrayList<Pedido> pedidos, int numero_mesa){
        String sql2 = "DELETE FROM pedidos WHERE numero_mesa = ?;";
        String sql = "INSERT INTO pedidos (id_pedido, nombre, precio, cantidad, total, numero_mesa) VALUES (?,?,?,?,?,?);";
        
        try {
            con = cn.MySQLConnect();
            ps = con.prepareStatement(sql2);
            ps.setInt(1, numero_mesa);
            ps.execute();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }
        
        for(int i =0; i < pedidos.size(); i++){
            try {
                con = cn.MySQLConnect();
                ps = con.prepareStatement(sql);
                ps.setInt(1, pedidos.get(i).getId());
                ps.setString(2, pedidos.get(i).getNombre());
                ps.setDouble(3, pedidos.get(i).getPrecio());
                ps.setInt(4, pedidos.get(i).getCantidad());
                ps.setDouble(5, pedidos.get(i).getTotal());
                ps.setInt(6, numero_mesa);
                ps.execute();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e.toString());
            }
        }   
    }
    
    public ArrayList<Pedido> ObtenerPedidos(int numero_mesa){
        ArrayList<Pedido> pedidos = new ArrayList();
        String sql = "SELECT * FROM pedidos WHERE numero_mesa = ?;";
        Pedido pedido = new Pedido();
        try {
            con = cn.MySQLConnect();
            ps = con.prepareStatement(sql);
            ps.setInt(1,numero_mesa);
            rs = ps.executeQuery();
            while(rs.next()){
                pedido = new Pedido(rs.getInt("id_pedido"),
                                    rs.getString("nombre"),
                                    rs.getDouble("precio"),
                                    rs.getInt("cantidad"),
                                    rs.getDouble("total")
                );
                pedidos.add(pedido);
                
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }
        return pedidos;
    }

    public int id_orden(){
        int id = 0;
        String sql = "SELECT MAX(id) AS id FROM ordenes;";
        try {
            con = cn.MySQLConnect();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            if(rs.next()){
                id = rs.getInt("id");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return id;
    }

    public ArrayList<Pedido> ArrayDetalle(int numero_mesa, int numero_orden){
            ArrayList<Pedido> pedidos = new ArrayList();
            String sql = "SELECT * FROM pedidos WHERE numero_mesa = ? AND orden_id = ?;";
            Pedido pedido = new Pedido();
            try {
                con = cn.MySQLConnect();
                ps = con.prepareStatement(sql);
                ps.setInt(1,numero_mesa);
                ps.setInt(2,numero_orden);
                rs = ps.executeQuery();
                while(rs.next()){
                    pedido = new Pedido(rs.getString("nombre"),
                                        rs.getInt("cantidad"),
                                        rs.getDouble("total")
                    );
                    pedidos.add(pedido);
                }
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.toString());
            }
            return pedidos;
        }

    public Boolean CompraFinalizada( int usuario_id, int numero_mesa, double total, String metodo_pago){
        String sql = "INSERT INTO ordenes (numero_mesa,total,metodo_pago,usuario_id) VALUES (?,?,?,?);";
        try {
            con = cn.MySQLConnect();
            ps = con.prepareStatement(sql);
            ps.setInt(1, numero_mesa);
            ps.setDouble(2, total);
            ps.setString(3, metodo_pago);
            ps.setInt(4, usuario_id);
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
            return false;
        }
    }
    
    public Boolean AgregarProductosComprados(ArrayList<Pedido> compraFinalizada, int id_orden, int numero_mesa){
        String sql = "INSERT INTO platilloscomprados (nombre, cantidad, total, numero_mesa ,orden_id) VALUES (?,?,?,?,?);";
        for(int i = 0; i < compraFinalizada.size(); i++){
            try {
                ps = con.prepareStatement(sql);
                ps.setString(1, compraFinalizada.get(i).getNombre());
                ps.setInt(2, compraFinalizada.get(i).getCantidad());
                ps.setDouble(3, compraFinalizada.get(i).getTotal());
                ps.setInt(4, numero_mesa);
                ps.setInt(5, id_orden);
                ps.execute();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.toString());
                return false;
            }
        }
        return true;
    }
    
    public Boolean EliminarPedidosAnteriores(int numero_mesa){
        String sql = "DELETE FROM pedidos WHERE numero_mesa = ?;";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1,numero_mesa);
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
            return false;
        }
    }

    public Boolean EliminarPedido(Pedido pedido){
        String sql = "DELETE FROM pedidos WHERE id_pedido = ?;";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1,pedido.getId());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
            return false;
        }
    }
}

        