/*
    Este archivo contiene los datos necesarios para crear el modelo "Empelado":
    Sus atributos son los siguientes:

    [x] Id
    [x] nombre
    [x] fecha de nacimiento
    [x] email
    [x] usuario
    [x] password
    [x] rol
    [x] foto

    [x] Contiene sus constructores para mandarlo a llamar desde otras clases.
    [x] contiene sus setters y getters.
    
*/
package Modelo;

public class Empleado {
    
    private int id;
    private String nombre;
    private String fechaNacimiento;
    private String email;
    private String usuario;
    private String password;
    private String rol;
    byte[] foto;
    
    /*
        Constructores
    */
    
    public Empleado(){
        
    }
    
    public Empleado(int id, String nombre, String fechaNacimiento, String email, String usuario, String password, String rol, byte[] foto) {
        this.id = id;
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.email = email;
        this.usuario = usuario;
        this.password = password;
        this.rol = rol;
        this.foto = foto;
    }

    public Empleado(int id) {
        this.id = id;
    }

    public Empleado(int id, String nombre, String fechaNacimiento, String email, String usuario, String password, String rol) {
        this.id = id;
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.email = email;
        this.usuario = usuario;
        this.password = password;
        this.rol = rol;
    }

    public Empleado(String nombre, byte[] foto, String fechaNacimiento, String email, String usuario, String password, String rol) {
        this.nombre = nombre;
        this.foto = foto;
        this.fechaNacimiento = fechaNacimiento;
        this.email = email;
        this.usuario = usuario;
        this.password = password;
        this.rol = rol;
    }
    
    /*
        Setters y Getters
    */
    
    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

}
