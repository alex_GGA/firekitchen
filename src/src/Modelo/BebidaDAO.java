/*
    Este clase se encarga de realizar todos los querys que necesitamos directamente a la base de datos y contiene las siguientes funciones.

    [x] Registrar una nueva Bebida en la base de datos.
    [x] Modificar una Bebida en la base de datos.
    [x] Eliminar una Bebida en la base de datos.
    [x] Recuperar todas las bebidas almacenadas en la base de datos.
*/
package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class BebidaDAO {
    // Variables que necesitamos:
    
    Conexion cn = new Conexion(); // Mandamos a llamar a nuestra clase que realiza la conexión a la base de datos.
    Connection con;               // Variable para ejecutar la conexión  
    PreparedStatement ps;         // Variable para guardar el query y ejecutarlo.
    ResultSet rs;                 // Variable para almacenar los resultados.
    
    public boolean RegistrarBebida(Bebida bebida) {
        String sql = "INSERT INTO bebidas (nombre,precio,categoria,imagen,usuario_id) VALUES (?,?,?,?,?);";
        try {
            con = cn.MySQLConnect();
            ps = con.prepareStatement(sql);
            ps.setString(1, bebida.getNombre());
            ps.setString(2, bebida.getPrecio());
            ps.setString(3, bebida.getCategoria());
            ps.setBytes(4, bebida.getFoto());
            ps.setInt(5, bebida.getUsuario_id());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
            return false;
        }finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
    }

    public boolean ModificarBebida(Bebida bebida){
        String sql;
        try{
            con = cn.MySQLConnect();
            if(bebida.getFoto() != null){
                sql = "UPDATE bebidas SET nombre = ?, precio = ?, categoria= ?, imagen= ?, usuario_id= ? WHERE id = ?;";
                ps = con.prepareStatement(sql);
                ps.setString(1, bebida.getNombre());
                ps.setString(2, bebida.getPrecio());
                ps.setString(3, bebida.getCategoria());
                ps.setBytes(4, bebida.getFoto());
                ps.setInt(5, bebida.getUsuario_id());
                ps.setInt(6, bebida.getId());
            }else{
                sql = "UPDATE bebidas SET nombre = ?, precio = ?, categoria= ?, usuario_id= ? WHERE id = ?;";
                ps = con.prepareStatement(sql);
                ps.setString(1, bebida.getNombre());
                ps.setString(2, bebida.getPrecio());
                ps.setString(3, bebida.getCategoria());
                ps.setInt(4, bebida.getUsuario_id());
                ps.setInt(5, bebida.getId());
            }
            ps.execute();
            return true;
        }catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
            return false;
        }finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
    }
    
    public boolean EliminarBebida(Bebida bebida){
        String sql = "DELETE FROM bebidas WHERE id = ?;";
        try {
            con = cn.MySQLConnect();
            ps = con.prepareStatement(sql);
            ps.setInt(1, bebida.getId());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
            return false;
        }finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
    }
    
    public ArrayList<Bebida> ListaBebida(String valor){
        ArrayList<Bebida> ListaBebida = new ArrayList();
        String sql = "SELECT * FROM bebidas;";
        String sqlBuscar = "SELECT * FROM bebidas WHERE nombre LIKE '%"+valor+"%' OR id LIKE '%"+valor+"%' OR categoria LIKE '%"+valor+"%';";
        try {
            con = cn.MySQLConnect();
            if(valor.equalsIgnoreCase("")){
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
            }else{
                ps = con.prepareStatement(sqlBuscar);
                rs = ps.executeQuery(); 
            }
            while(rs.next()){
                Bebida bebida = new Bebida(rs.getInt("id"),
                                                rs.getString("nombre"),
                                                rs.getString("precio"),
                                                rs.getString("categoria"),
                                                rs.getBytes("imagen"),
                                                rs.getInt("usuario_id")     
                );
                ListaBebida.add(bebida);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }
        return ListaBebida;
    }
}
