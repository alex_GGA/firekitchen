/*
    Este archivo contiene los datos necesarios para crear el modelo "pedido":
    Sus atributos son los siguientes:

    [x] Id
    [x] nombre
    [x] precio
    [x] cantidad
    
    [x] Contiene sus constructores para mandarlo a llamar desde otras clases.
    [x] contiene sus setters y getters.
    
*/
package Modelo;


public class Pedido {
    private int id;
    private String nombre;
    private Double precio;
    private int cantidad;
    private Double total;

    /*
        Constructores
    */
    
    public Pedido() {
    }
    
    public Pedido(int id, String nombre, Double precio, int cantidad, Double total) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.cantidad = cantidad;
        this.total = total;
    }

    public Pedido(String nombre, int cantidad, Double total) {
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.total = total;
    }
    
    /*
        Setters y Getters
    */
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
    
    
}
