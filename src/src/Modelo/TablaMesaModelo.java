/*
    Modelo que utilizaremos para la tabla de mesas, en donde se van a almacenar los platillo que vaya a gregando el mesero..
*/
package Modelo;

import javax.swing.table.AbstractTableModel;

public class TablaMesaModelo extends AbstractTableModel {

    private String[] columnas;
    private Object[][] filas;

    public TablaMesaModelo() {
    }
    
    public TablaMesaModelo(Object[][] datos, String[] nombreColumna) {
        this.columnas = nombreColumna;
        this.filas = datos;
    } 
    
    @Override
    public int getRowCount() {
        return this.filas.length;
    }

    @Override
    public int getColumnCount() {
        return this.columnas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return this.filas[rowIndex][columnIndex];
    }
    
    @Override
    public String getColumnName(int col){
        return this.columnas[col];
    }
    
    @Override
    public boolean isCellEditable(int row, int column){  
        return false;  
    }
    
}
