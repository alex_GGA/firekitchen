/*
    Este clase se encarga de realizar todos los querys que necesitamos directamente a la base de datos y contiene las siguientes funciones.

    [x] Registrar un nuevo empleado en la base de datos.
    [x] Modificar un empleado en la base de datos.
    [x] Eliminar un empleado en la base de datos.
    [x] Recuperar todos lod empleados almacenados en la base de datos.
*/
package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;


public class EmpleadoDAO {
    // Variables que necesitamos:
    
    Conexion cn = new Conexion(); // Mandamos a llamar a nuestra clase que realiza la conexión a la base de datos.
    Connection con;               // Variable para ejecutar la conexión  
    PreparedStatement ps;         // Variable para guardar el query y ejecutarlo.
    ResultSet rs;                 // Variable para almacenar los resultados.
   
    public boolean RegistrarEmpleado(Empleado empleado) {
        String sql = "INSERT INTO usuario (nombre,fecha_nacimiento,correo,usuario,password,cargo,foto) VALUES (?,?,?,?,?,?,?);";
        try {
            con = cn.MySQLConnect();
            ps = con.prepareStatement(sql);
            ps.setString(1, empleado.getNombre());
            ps.setString(2, empleado.getFechaNacimiento());
            ps.setString(3, empleado.getEmail());
            ps.setString(4, empleado.getUsuario());
            ps.setString(5, empleado.getPassword());
            ps.setString(6, empleado.getRol());
            ps.setBytes(7, empleado.getFoto());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "El email o nombre de usuario deben de ser unicos.");
            return false;
        }finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
    }
    
    public boolean ModificarEmpleado(Empleado empleado){
        String sql;
        try {
            con = cn.MySQLConnect();
            if(empleado.getFoto() != null){
                sql = "UPDATE usuario SET  nombre = ?, fecha_nacimiento = ?, correo = ?, usuario = ?, password = ?, cargo = ?, foto = ? WHERE id = ?;";
                ps = con.prepareStatement(sql);
                ps.setString(1, empleado.getNombre());
                ps.setString(2, empleado.getFechaNacimiento());
                ps.setString(3, empleado.getEmail());
                ps.setString(4, empleado.getUsuario());
                ps.setString(5, empleado.getPassword());
                ps.setString(6, empleado.getRol());
                ps.setBytes(7, empleado.getFoto());
                ps.setInt(8, empleado.getId());
            }else{
                sql = "UPDATE usuario SET  nombre = ?, fecha_nacimiento = ?, correo = ?, usuario = ?, password = ?, cargo = ? WHERE id = ?;";
                ps = con.prepareStatement(sql);
                ps.setString(1, empleado.getNombre());
                ps.setString(2, empleado.getFechaNacimiento());
                ps.setString(3, empleado.getEmail());
                ps.setString(4, empleado.getUsuario());
                ps.setString(5, empleado.getPassword());
                ps.setString(6, empleado.getRol());
                ps.setInt(7, empleado.getId());
            }
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "El email o nombre de usuario deben de ser unicos.");
            return false;
        }finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
    }

    public boolean EliminarEmpleado(Empleado empleado){
        String sql = "DELETE FROM usuario WHERE id = ?;";
        try {
            con = cn.MySQLConnect();
            ps = con.prepareStatement(sql);
            ps.setInt(1, empleado.getId());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
            return false;
        }finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
    }

    public ArrayList<Empleado> ListaEmpleados(String valor){
        ArrayList<Empleado> ListaEmpleado = new ArrayList();
        String sql = "SELECT * FROM usuario;";
        String sqlBuscar = "SELECT * FROM usuario WHERE usuario LIKE '%"+valor+"%' OR nombre LIKE '%"+valor+"%' OR cargo LIKE '%"+valor+"%' OR id LIKE '%"+valor+"%';";
        try {
            con = cn.MySQLConnect();
            if(valor.equalsIgnoreCase("")){
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
            }else{
                ps = con.prepareStatement(sqlBuscar);
                rs = ps.executeQuery(); 
            }
            
            
            while(rs.next()){
                Empleado empleado = new Empleado(rs.getInt("id"),
                                                rs.getString("nombre"),
                                                rs.getString("fecha_nacimiento"),
                                                rs.getString("correo"),
                                                rs.getString("usuario"),
                                                rs.getString("password"),
                                                rs.getString("cargo"),
                                                rs.getBytes("foto")
                );
                ListaEmpleado.add(empleado);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }
        return ListaEmpleado;
    }
}
