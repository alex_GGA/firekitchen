/*
    Este clase se encarga de realizar todos los querys que necesitamos directamente a la base de datos y contiene las siguientes funciones.

    [x] login
*/
package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;


public class LoginDAO {
    // Variables que necesitamos:
    
    Conexion cn = new Conexion(); // Mandamos a llamar a nuestra clase que realiza la conexión a la base de datos.
    Connection con;               // Variable para ejecutar la conexión  
    PreparedStatement ps;         // Variable para guardar el query y ejecutarlo.
    ResultSet rs;                 // Variable para almacenar los resultados.
    
    public Empleado login(String usuario, String password){
        
        String query = "SELECT * FROM usuario WHERE usuario = ? AND password = ?";
        Empleado usuario_buscado = new Empleado();
        
        try {
            con = cn.MySQLConnect();
            ps = con.prepareStatement(query);
            ps.setString(1, usuario);
            ps.setString(2, password);
            rs = ps.executeQuery();
            
            if(rs.next()){
                if(rs.getString("usuario").equals(usuario) && rs.getString("password").equals(password)){
                    usuario_buscado.setId(rs.getInt("id"));
                    usuario_buscado.setNombre(rs.getString("nombre"));
                    usuario_buscado.setFechaNacimiento(rs.getString("fecha_nacimiento"));
                    usuario_buscado.setEmail(rs.getString("correo"));
                    usuario_buscado.setUsuario(rs.getString("usuario"));
                    usuario_buscado.setPassword(rs.getString("password"));
                    usuario_buscado.setRol(rs.getString("cargo"));
                }
            }
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }
        return usuario_buscado;
    }
}
