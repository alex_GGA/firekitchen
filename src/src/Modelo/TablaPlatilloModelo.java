/*
    Modelo que utilizaremos para la tabla de platillo.
    Lo hicimos de esta manera por que necesitabamos cambiar el tipo de la columan 4 a Tipo Icon.
*/
package Modelo;

import javax.swing.Icon;
import javax.swing.table.AbstractTableModel;

public class TablaPlatilloModelo extends AbstractTableModel {

    private String[] columnas;
    private Object[][] filas;

    public TablaPlatilloModelo() {
    }
    
    public TablaPlatilloModelo(Object[][] datos, String[] nombreColumna){
        this.columnas = nombreColumna;
        this.filas = datos;
    }
    
    @Override
    public Class getColumnClass(int col){
        if(col == 4){
            return Icon.class;
        }
        else{
            return getValueAt(0, col).getClass();
        }
    }
    
    @Override
    public int getRowCount() {
        return this.filas.length;
    }

    @Override
    public int getColumnCount() {
        return this.columnas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return this.filas[rowIndex][columnIndex];
    }
    
    @Override
    public String getColumnName(int col){
        return this.columnas[col];
    }
    
}
