/*
    Este clase se encarga de realizar todos los querys que necesitamos directamente a la base de datos y contiene las siguientes funciones.

    [x] Registrar un nuevo plattilo en la base de datos.
    [x] Modificar un plattilo en la base de datos.
    [x] Eliminar un plattilo en la base de datos.
    [x] Recuperar todos los plattilos almacenados en la base de datos.
*/
package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class PlatilloDAO {
    // Variables que necesitamos:
    
    Conexion cn = new Conexion(); // Mandamos a llamar a nuestra clase que realiza la conexión a la base de datos.
    Connection con;               // Variable para ejecutar la conexión  
    PreparedStatement ps;         // Variable para guardar el query y ejecutarlo.
    ResultSet rs;                 // Variable para almacenar los resultados.
    
    public boolean RegistrarPlatillo(Platillo platillo) {
        String sql = "INSERT INTO platillos (nombre,precio,categoria,imagen,usuario_id) VALUES (?,?,?,?,?);";
        try {
            con = cn.MySQLConnect();
            ps = con.prepareStatement(sql);
            ps.setString(1, platillo.getNombre());
            ps.setString(2, platillo.getPrecio());
            ps.setString(3, platillo.getCategoria());
            ps.setBytes(4, platillo.getFoto());
            ps.setInt(5, platillo.getUsuario_id());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
            return false;
        }finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
    }

    public boolean ModificarPlatillo(Platillo platillo){
        String sql;
        try{
            con = cn.MySQLConnect();
            if(platillo.getFoto() != null){
                sql = "UPDATE platillos SET nombre = ?, precio = ?, categoria= ?, imagen= ?, usuario_id= ? WHERE id = ?;";
                ps = con.prepareStatement(sql);
                ps.setString(1, platillo.getNombre());
                ps.setString(2, platillo.getPrecio());
                ps.setString(3, platillo.getCategoria());
                ps.setBytes(4, platillo.getFoto());
                ps.setInt(5, platillo.getUsuario_id());
                ps.setInt(6, platillo.getId());
            }else{
                sql = "UPDATE platillos SET nombre = ?, precio = ?, categoria= ?, usuario_id= ? WHERE id = ?;";
                ps = con.prepareStatement(sql);
                ps.setString(1, platillo.getNombre());
                ps.setString(2, platillo.getPrecio());
                ps.setString(3, platillo.getCategoria());
                ps.setInt(4, platillo.getUsuario_id());
                ps.setInt(5, platillo.getId());
            }
            ps.execute();
            return true;
        }catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
            return false;
        }finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
    }
    
    public boolean EliminarPlatillo(Platillo platillo){
        String sql = "DELETE FROM platillos WHERE id = ?;";
        try {
            con = cn.MySQLConnect();
            ps = con.prepareStatement(sql);
            ps.setInt(1, platillo.getId());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
            return false;
        }finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
    }
    
    public ArrayList<Platillo> ListaPlatillo(String valor){
        ArrayList<Platillo> ListaPlatillo = new ArrayList();
        String sql = "SELECT * FROM platillos;";
        String sqlBuscar = "SELECT * FROM platillos WHERE nombre LIKE '%"+valor+"%' OR id LIKE '%"+valor+"%' OR categoria LIKE '%"+valor+"%';";
        try {
            con = cn.MySQLConnect();
            if(valor.equalsIgnoreCase("")){
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
            }else{
                ps = con.prepareStatement(sqlBuscar);
                rs = ps.executeQuery(); 
            }
            while(rs.next()){
                Platillo platillo = new Platillo(rs.getInt("id"),
                                                rs.getString("nombre"),
                                                rs.getString("precio"),
                                                rs.getString("categoria"),
                                                rs.getBytes("imagen"),
                                                rs.getInt("usuario_id")     
                );
                ListaPlatillo.add(platillo);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }
        return ListaPlatillo;
    }
}
