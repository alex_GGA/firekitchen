/*
    Esta clase de tipo controlador, se encargara de controlar la vista de el menu de las bebidas,
    el cual se encuentra en el menu del gerente.
*/
package Controlador;

import Modelo.Bebida;
import Modelo.BebidaDAO;
import Modelo.TablaBebidaModelo;
import Vista.Gerente_vista;
import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.JTableHeader;

public class BebidaController implements ActionListener,MouseListener ,KeyListener {
    private Bebida bebida;
    private BebidaDAO bebidaDAO;
    private Gerente_vista vista;
    
    // Constructor del controlador.
    
    public BebidaController(Bebida bebida, BebidaDAO bebidaDAO, Gerente_vista vista) {
        this.bebida = bebida;
        this.bebidaDAO = bebidaDAO;
        this.vista = vista;
        this.vista.btn_NuevaBebida.addActionListener(this);
        this.vista.btn_GuardarBebida.addActionListener(this);
        this.vista.btn_CancelarBebida.addActionListener(this);
        this.vista.btn_ModificarBebida.addActionListener(this);
        this.vista.btn_actualizarBebida.addActionListener(this);
        this.vista.btn_buscarImagenBebida.addActionListener(this);
        this.vista.jMenuEliminarBebida.addActionListener(this);
        this.vista.txt_buscarBebida.addKeyListener(this);
        this.vista.jLabelBebida.addMouseListener(this);
        this.vista.tablaBebidas.addMouseListener(this);
        tablaBebida();
    }

    public void limpiarTxt(){
        vista.txt_idBebida.setText("");
        vista.txt_nombreBebida.setText("");
        vista.txt_PrecioBebida.setText("");
        vista.cb_categoriaBebida.setSelectedIndex(0);
        vista.jLabel_imagenBebida.setIcon(null);
        vista.txt_rutaImagenBebida.setText("");
    }
    
    public void bloquearTxt(){
        vista.txt_idBebida.setEnabled(false);
        vista.txt_nombreBebida.setEnabled(false);
        vista.txt_PrecioBebida.setEnabled(false);
        vista.cb_categoriaBebida.setEnabled(false);
        vista.btn_buscarImagenBebida.setEnabled(false);
    }
    
    public void desbloquearTxt(){
        vista.txt_nombreBebida.setEnabled(true);
        vista.txt_PrecioBebida.setEnabled(true);
        vista.cb_categoriaBebida.setEnabled(true);
        vista.btn_buscarImagenBebida.setEnabled(true);
    }
    
    public void predeterminado(){
        limpiarTxt();
        bloquearTxt();
        vista.btn_NuevaBebida.setEnabled(true);
        vista.btn_GuardarBebida.setEnabled(false);
        vista.btn_ModificarBebida.setEnabled(false);
        vista.btn_actualizarBebida.setEnabled(false);
        vista.btn_CancelarBebida.setEnabled(false);
    }
    
    public void btnNuevaBebida(){
        desbloquearTxt();
        vista.btn_NuevaBebida.setEnabled(false);
        vista.btn_GuardarBebida.setEnabled(true);
        vista.btn_ModificarBebida.setEnabled(false);
        vista.btn_actualizarBebida.setEnabled(false);
        vista.btn_CancelarBebida.setEnabled(true);
    }

    public void btnActualizarBebida(){
        desbloquearTxt();
        vista.btn_NuevaBebida.setEnabled(false);
        vista.btn_GuardarBebida.setEnabled(false);
        vista.btn_ModificarBebida.setEnabled(false);
        vista.btn_actualizarBebida.setEnabled(true);
        vista.btn_CancelarBebida.setEnabled(true);
    }
    
    public Boolean validaciones (String nombre, String precio){
        Pattern NOMBRE_REGEX = Pattern.compile("^([A-Za-zñáéíóúÑÁÉÍÓÚ]+\\s?)*$",Pattern.CASE_INSENSITIVE);
        Pattern PRECIO_REGEX = Pattern.compile("^\\d*.?\\d*$");
        
        Matcher matcher_nombre = NOMBRE_REGEX.matcher(nombre);
        Matcher matcher_precio = PRECIO_REGEX.matcher(precio);
        
        if(matcher_nombre.find() == true){
            if(matcher_precio.find() == true){
                return true;
            }else{
                vista.txt_PrecioBebida.setText("");
                JOptionPane.showMessageDialog(null, "Formato del precio incorrecto.");
                return false;
            }
        }else{
            vista.txt_nombreBebida.setText("");
            JOptionPane.showMessageDialog(null, "Formato del nombre incorrecto.");
            return false;
        }
    }
    
    public void GuardarImagen(){
        String ruta;
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("PNG","JPG","png","jpg");
        JFileChooser imagen = new JFileChooser();
        imagen.setFileFilter(filtro);
        imagen.setMultiSelectionEnabled(false);
        int opcion = imagen.showOpenDialog(vista);
        if(opcion == JFileChooser.APPROVE_OPTION){
            ruta = imagen.getSelectedFile().getAbsolutePath();
            Image preview = Toolkit.getDefaultToolkit().getImage(ruta);
            vista.txt_rutaImagenBebida.setText(ruta);
            if(preview != null){
                vista.jLabel_imagenBebida.setText("");
                ImageIcon icono = new ImageIcon(preview.getScaledInstance(100,100, Image.SCALE_SMOOTH));
                vista.jLabel_imagenBebida.setIcon(icono);
            }
        }
    }
    
    public void tablaBebida(){
        ArrayList<Bebida> bebidas = bebidaDAO.ListaBebida(vista.txt_buscarBebida.getText());
        String[] colNombres = {"id","Nombre","Precio", "Categoria", "Imagen", "Usuario id"};
        Object[][] filas = new Object[bebidas.size()][8];
        for(int i = 0; i < bebidas.size(); i++ ){
            filas[i][0] = bebidas.get(i).getId();
            filas[i][1] = bebidas.get(i).getNombre();
            filas[i][2] = bebidas.get(i).getPrecio();
            filas[i][3] = bebidas.get(i).getCategoria();
            ImageIcon img = new ImageIcon(new ImageIcon(bebidas.get(i).getFoto()).getImage().getScaledInstance(100,100, Image.SCALE_SMOOTH));
            filas[i][4] = img;
            filas[i][5] = bebidas.get(i).getUsuario_id();
        }
        TablaBebidaModelo tablaModelo = new TablaBebidaModelo(filas, colNombres);
        vista.tablaBebidas.setModel(tablaModelo);
        JTableHeader encabezado = vista.tablaBebidas.getTableHeader();
        encabezado.setOpaque(false);
        encabezado.setBackground(new Color(240,240,240));
        encabezado.setForeground(Color.black);
        vista.tablaBebidas.setRowHeight(vista.jLabel_imagenBebida.getHeight()); 
    }
    
    @Override
    public void actionPerformed(ActionEvent evento) {
        if(evento.getSource() == vista.btn_NuevaBebida){
            limpiarTxt();
            btnNuevaBebida();
        }
        else if(evento.getSource() == vista.btn_ModificarBebida){
            btnActualizarBebida();
        }
        else if(evento.getSource() == vista.btn_CancelarBebida){
            tablaBebida();
            predeterminado();
        }
        else if(evento.getSource() == vista.btn_buscarImagenBebida){
            GuardarImagen();
        }
        else if(evento.getSource() == vista.btn_GuardarBebida){
            if(vista.txt_nombreBebida.getText().equals("") || vista.txt_PrecioBebida.getText().equals("") || vista.cb_categoriaBebida.getSelectedItem().equals("-") || vista.txt_rutaImagenBebida.getText().equals("")){
                JOptionPane.showMessageDialog(null, "Todos los campos son obligatorios.");
                return;
            }
            if(validaciones(vista.txt_nombreBebida.getText(), vista.txt_PrecioBebida.getText())){
                try {
                    bebida = new Bebida(vista.txt_nombreBebida.getText(),vista.txt_PrecioBebida.getText(), vista.cb_categoriaBebida.getSelectedItem().toString() ,Files.readAllBytes(Paths.get(vista.txt_rutaImagenBebida.getText())), 29);
                    if(bebidaDAO.RegistrarBebida(bebida)){
                        tablaBebida();
                        predeterminado();
                        JOptionPane.showMessageDialog(null, "Bebida guardada con exito.");
                    }else{
                        limpiarTxt();
                        JOptionPane.showMessageDialog(null, "Hubo un error al guardar la bebida.");
                    }
                } catch (IOException ex) {
                    Logger.getLogger(BebidaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        else if(evento.getSource() == vista.btn_actualizarBebida){
            try {
                if(vista.txt_nombreBebida.getText().equals("") || vista.txt_PrecioBebida.getText().equals("") || vista.cb_categoriaBebida.getSelectedItem().equals("-")){
                    JOptionPane.showMessageDialog(null, "Todos los campos son obligatorios.");
                    return;
                }
                if(validaciones(vista.txt_nombreBebida.getText(), vista.txt_PrecioBebida.getText())){
                    if(vista.txt_rutaImagenBebida.getText().equals("")){
                        bebida = new Bebida(Integer.valueOf(vista.txt_idBebida.getText()),vista.txt_nombreBebida.getText(),vista.txt_PrecioBebida.getText(), vista.cb_categoriaBebida.getSelectedItem().toString(), 29);
                    }else{
                        bebida = new Bebida(Integer.valueOf(vista.txt_idBebida.getText()),vista.txt_nombreBebida.getText(),vista.txt_PrecioBebida.getText(), vista.cb_categoriaBebida.getSelectedItem().toString(),Files.readAllBytes(Paths.get(vista.txt_rutaImagenBebida.getText())) ,29);
                    }
                    if(bebidaDAO.ModificarBebida(bebida)){
                        tablaBebida();
                        predeterminado();
                        JOptionPane.showMessageDialog(null, "Bebida actualizada con exito.");
                    }else{
                        limpiarTxt();
                        JOptionPane.showMessageDialog(null, "Hubo un error al actualizar la bebida.");
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(BebidaController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if(evento.getSource() == vista.jMenuEliminarBebida){
            if(vista.txt_idBebida.getText().equals("")){
                JOptionPane.showMessageDialog(null, "Seleccione una fila para eliminar.");
            }else{
                bebida = new Bebida(Integer.valueOf(vista.txt_idBebida.getText()));
                int preguntaSalir = JOptionPane.showConfirmDialog(null, "¿Estas seguro que quieres eliminar esta bebida?", "Pregunta", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (preguntaSalir == 0){
                    if(bebidaDAO.EliminarBebida(bebida)){
                        predeterminado();
                        tablaBebida();
                        JOptionPane.showMessageDialog(null, "Bebida eliminada con exito.");
                    } else{
                        limpiarTxt();
                        JOptionPane.showMessageDialog(null, "Hubo un error al eliminar la bebida.");
                    }
                }
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource() == vista.tablaBebidas){
            int fila = vista.tablaBebidas.rowAtPoint(e.getPoint());
            vista.txt_idBebida.setText(vista.tablaBebidas.getValueAt(fila, 0).toString());
            vista.txt_nombreBebida.setText(vista.tablaBebidas.getValueAt(fila, 1).toString());
            vista.txt_PrecioBebida.setText(vista.tablaBebidas.getValueAt(fila, 2).toString());
            vista.cb_categoriaBebida.setSelectedItem(vista.tablaBebidas.getValueAt(fila, 3).toString());
            vista.jLabel_imagenBebida.setIcon((Icon) vista.tablaBebidas.getValueAt(fila, 4));
            bloquearTxt();
            vista.btn_NuevaBebida.setEnabled(true);
            vista.btn_GuardarBebida.setEnabled(false);
            vista.btn_ModificarBebida.setEnabled(true);
            vista.btn_actualizarBebida.setEnabled(false);
            vista.btn_CancelarBebida.setEnabled(true);
        }else if(e.getSource() == vista.jLabelBebida){
            vista.tabbed_Opciones.setSelectedIndex(3);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == vista.txt_buscarBebida){
            tablaBebida();
        }
    }
    
    
}
