/*
    Esta clase de tipo controlador, se encargara de controlar y asignar funciones a los paneles del menu de mesero.
*/
package Controlador;

import Modelo.Empleado;
import Modelo.MesaDAO;
import Vista.Mesero_vista;
import Vista.login;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public class ConfiguracionMenuMeseroController implements MouseListener {
    
    private Mesero_vista vista;
    private MesaDAO mesaDAO;

    public ConfiguracionMenuMeseroController(Mesero_vista vista, Empleado empleado) {
        this.vista = vista;
        this.vista.setLocationRelativeTo(null);
        this.vista.setExtendedState(vista.MAXIMIZED_BOTH);
        this.vista.txt_empleadoNombre.setText(empleado.getNombre());
        this.vista.jLabel_Mesa1.addMouseListener(this);
        this.vista.jLabel_Mesa2.addMouseListener(this);
        this.vista.jLabel_Mesa3.addMouseListener(this);
        this.vista.jLabel_Mesa4.addMouseListener(this);
        this.vista.jLabel_Mesa5.addMouseListener(this);
        this.vista.jLabel_Mesa6.addMouseListener(this);
        this.vista.jLabel_Mesa7.addMouseListener(this);
        this.vista.jLabel_Mesa8.addMouseListener(this);
        this.vista.jLabel_Mesa9.addMouseListener(this);
        this.vista.jLabel_Mesa10.addMouseListener(this);
        this.vista.jLabel_Mesa11.addMouseListener(this);
        this.vista.jLabel_Mesa12.addMouseListener(this);
        this.vista.jLabelCerrarSesión.addMouseListener(this);
        this.vista.jLabelInicio.addMouseListener(this);
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource() == vista.jLabel_Mesa1){
            vista.MenuMesero.setSelectedIndex(1);
        }else if(e.getSource() == vista.jLabel_Mesa2){
            vista.MenuMesero.setSelectedIndex(2);
        }else if(e.getSource() == vista.jLabel_Mesa3){
            vista.MenuMesero.setSelectedIndex(3);
        }else if(e.getSource() == vista.jLabel_Mesa4){
            vista.MenuMesero.setSelectedIndex(4);
        }else if(e.getSource() == vista.jLabel_Mesa5){
            vista.MenuMesero.setSelectedIndex(5);
        }else if(e.getSource() == vista.jLabel_Mesa6){
            vista.MenuMesero.setSelectedIndex(6);
        }else if(e.getSource() == vista.jLabel_Mesa7){
            vista.MenuMesero.setSelectedIndex(7);
        }else if(e.getSource() == vista.jLabel_Mesa8){
            vista.MenuMesero.setSelectedIndex(8);
        }else if(e.getSource() == vista.jLabel_Mesa9){
            vista.MenuMesero.setSelectedIndex(9);
        }else if(e.getSource() == vista.jLabel_Mesa10){
            vista.MenuMesero.setSelectedIndex(10);
        }else if(e.getSource() == vista.jLabel_Mesa11){
           vista.MenuMesero.setSelectedIndex(11);
        }else if(e.getSource() == vista.jLabel_Mesa12){
            vista.MenuMesero.setSelectedIndex(12);
        }else if(e.getSource() == vista.jLabelCerrarSesión){
            login loginVista = new login();
            loginVista.setVisible(true);
            this.vista.dispose();
        }if(e.getSource() == vista.jLabelInicio){
            vista.MenuMesero.setSelectedIndex(0);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
    
}
