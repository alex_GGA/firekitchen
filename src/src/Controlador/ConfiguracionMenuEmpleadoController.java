/*
    Esta clase de tipo controlador, se encargara de controlar y asignar funciones a los paneles del menu de gerente.
*/
package Controlador;

import Vista.Gerente_vista;
import Vista.login;
import Modelo.Empleado;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ConfiguracionMenuEmpleadoController implements MouseListener{

    private Gerente_vista vista;
    private Empleado empleadoLogeado;

    public ConfiguracionMenuEmpleadoController(Gerente_vista vista, Empleado empleadoLogeado) {
        this.vista = vista;
        this.empleadoLogeado = empleadoLogeado;
        this.vista.txt_nombreGerente.setText(empleadoLogeado.getNombre());
        this.vista.jLabelEmpleado.addMouseListener(this);
        this.vista.jLabelPlatillo.addMouseListener(this);
        this.vista.jLabelBebida.addMouseListener(this);
        this.vista.jLabelCerrarSesión.addMouseListener(this);
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource() == vista.jLabelCerrarSesión){
            login loginVista = new login();
            loginVista.setVisible(true);
            this.vista.dispose();
        }
        
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if(e.getSource() == vista.jLabelEmpleado){
            vista.jPanelEmpleado.setBackground(new Color(255,255,255));
        }else if (e.getSource() == vista.jLabelPlatillo){
            vista.jPanelPlatillo.setBackground(new Color(255,255,255));
        }else if (e.getSource() == vista.jLabelBebida){
            vista.jPanelBebida.setBackground(new Color(255,255,255));
        }else {
            vista.jPanelCerrarSesión.setBackground(new Color(173,0,0));
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if(e.getSource() == vista.jLabelEmpleado){
            vista.jPanelEmpleado.setBackground(new Color(245,167,61));
        }else if (e.getSource() == vista.jLabelPlatillo){
            vista.jPanelPlatillo.setBackground(new Color(245,167,61));
        }else if (e.getSource() == vista.jLabelBebida){
            vista.jPanelBebida.setBackground(new Color(245,167,61));
        }else {
            vista.jPanelCerrarSesión.setBackground(new Color(255,0,0));
        }
    }
    
}
