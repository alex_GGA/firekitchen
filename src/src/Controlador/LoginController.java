/*
    Esta clase de tipo controlador, se encargara de que en caso el usuario le de click al boton "Iniciar sesión"
    pueda ingresar al menu de gerente o de menero, dependiendo del rol del empleado.
*/

package Controlador;

import Modelo.Empleado;
import Modelo.LoginDAO;
import Vista.Gerente_vista;
import Vista.login;
import Vista.Mesero_vista;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class LoginController implements ActionListener{
    
    private Empleado usuarioEncontrado;
    public Empleado usuarioLogeado;
    private LoginDAO usuarioConsulta;
    private login vista;

    public LoginController(Empleado usuario, LoginDAO usuarioConsulta, login vista) {
        this.usuarioEncontrado = usuario;
        this.usuarioConsulta = usuarioConsulta;
        this.vista = vista;
        this.vista.setLocationRelativeTo(null);
        this.vista.btn_login.addActionListener(this);
        this.vista.btn_cancelar.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent evento) {
        if (evento.getSource() == vista.btn_login) {
            if (vista.txt_usuario.getText().equals("") || String.valueOf(vista.txt_contraseña.getPassword()).equals("")) {
                JOptionPane.showMessageDialog(null, "Faltan campos por llenar");
            }else {
                String usuario = vista.txt_usuario.getText();
                String password = String.valueOf(vista.txt_contraseña.getPassword());
                usuarioEncontrado = usuarioConsulta.login(usuario, password);
                if (usuarioEncontrado.getUsuario() != null) {
                    if(usuarioEncontrado.getRol().equals("GERENTE")){
                        usuarioLogeado = usuarioEncontrado;
                        Gerente_vista menu = new Gerente_vista(usuarioEncontrado);
                        menu.setVisible(true);
                        this.vista.dispose();
                    }else{
                        usuarioLogeado = usuarioEncontrado;
                        Mesero_vista menu = new Mesero_vista(usuarioEncontrado);
                        menu.setVisible(true);
                        this.vista.dispose();
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Usuario o Contraseña incorrecta");
                    vista.txt_usuario.setText("");
                    vista.txt_contraseña.setText("");
                }
            }
        } else {
            int preguntaSalir = JOptionPane.showConfirmDialog(null, "¿Estas seguro que quieres salir?", "Pregunta", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (preguntaSalir == 0) System.exit(0);
        }
    
    }
}
