/*
    Esta clase de tipo controlador, se encargara de controlar la vista del ticket para que se pueda
    mostrar cuando una orden se haya finalizado.
*/
package Controlador;

import Modelo.MesaDAO;
import Modelo.Pedido;
import Vista.Ticket;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.PrintJob;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;


public class TicketController implements ActionListener {
    private MesaDAO mesaDAO = new MesaDAO();
    private Ticket vista = new Ticket();
    private double pagoTotal;
    
    DefaultTableModel modelo = new DefaultTableModel();

    public TicketController(Ticket vista, int id, ArrayList<Pedido> pedidos ,int id_empleado, double pago_cliente) {
        this.vista = vista;
        this.vista.setLocationRelativeTo(null);
        this.vista.btn_imprimir.addActionListener(this);
        this.vista.id_compra.setText("" + id);
        this.vista.txt_idEmpleado.setText(String.valueOf(id_empleado));
        this.listarDetalle(pedidos);
        this.vista.txt_ticketTotal.setText(SumaTotal());
        Cambio(pago_cliente);
    }
    
    public void listarDetalle(ArrayList<Pedido> pedidos){
        modelo = (DefaultTableModel) vista.ticketTable.getModel();
        Object[] ob = new Object[3];
        for(int i = 0; i < pedidos.size(); i++){
            ob[0] = pedidos.get(i).getCantidad();
            ob[1] = pedidos.get(i).getNombre();
            ob[2] = pedidos.get(i).getTotal();
            modelo.addRow(ob);
        }
        
        vista.ticketTable.setModel(modelo);
        JTableHeader encabezado = vista.ticketTable.getTableHeader();
        encabezado.setOpaque(false);
        encabezado.setBackground(new Color(240,240,240));
        encabezado.setForeground(Color.black);
        vista.ticketTable.setRowHeight(30);
    }
    
    public String SumaTotal(){
        double t = 0;
        double p = 0;
        if(vista.ticketTable.getRowCount() > 0){
            DecimalFormat df = new DecimalFormat("#.00");
            for(int i = 0; i < vista.ticketTable.getRowCount(); i++){
                p = Double.valueOf(vista.ticketTable.getValueAt(i, 2).toString());
                t += p;
                pagoTotal += p;
            }
            return "$" + String.valueOf(df.format(t));
        }
        return null;
    }
    
    public void Cambio(double pago_cliente){
        if(pago_cliente == 0){
            vista.txt_ticketCambio.setText("-");
        }else{
            DecimalFormat df = new DecimalFormat("#.00");
            
            System.out.println(pago_cliente);
            System.out.println(pagoTotal);
            vista.txt_ticketCambio.setText( "$" + String.valueOf(df.format(pago_cliente - pagoTotal)));
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.btn_imprimir){
            Toolkit tk = vista.ticketPanel.getToolkit();
            PrintJob pj = tk.getPrintJob(vista, null, null);
            Graphics g = pj.getGraphics();
            vista.ticketPanel.print(g);
            g.dispose();
            pj.end();
            this.vista.dispose();
        }
    }
    
}
