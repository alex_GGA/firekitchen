
package Controlador;

import Modelo.Platillo;
import Modelo.PlatilloDAO;
import Modelo.TablaPlatilloModelo;
import Vista.Gerente_vista;
import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.JTableHeader;

public class PlatilloController implements ActionListener,MouseListener ,KeyListener{
    private Platillo platillo;
    private PlatilloDAO platilloDAO;
    private Gerente_vista vista;

    public PlatilloController(Platillo platillo, PlatilloDAO platilloDAO, Gerente_vista vista) {
        this.platillo = platillo;
        this.platilloDAO = platilloDAO;
        this.vista = vista;
        this.vista.btn_NuevoPlatillo.addActionListener(this);
        this.vista.btn_GuardarPlatillo.addActionListener(this);
        this.vista.btn_CancelarPlatillo.addActionListener(this);
        this.vista.btn_ModificarPlatillo.addActionListener(this);
        this.vista.btn_actualizarPlatillo.addActionListener(this);
        this.vista.btn_buscarImagenPlatillo.addActionListener(this);
        this.vista.jMenu_EliminarPlatillo.addActionListener(this);
        this.vista.txt_buscarPlatillo.addKeyListener(this);
        this.vista.jLabelPlatillo.addMouseListener(this);
        this.vista.tablaPlatillos.addMouseListener(this);
        tablaPlatillo();
    }
    
    public void limpiarTxt(){
        vista.txt_idPlatillo.setText("");
        vista.txt_nombrePlatillo.setText("");
        vista.txt_PrecioPlatillo.setText("");
        vista.cb_categoriaPlatillo.setSelectedIndex(0);
        vista.jLabel_imagenPlatillo.setIcon(null);
        vista.txt_rutaImagenPlatillo.setText("");
    }
    
    public void bloquearTxt(){
        vista.txt_idPlatillo.setEnabled(false);
        vista.txt_nombrePlatillo.setEnabled(false);
        vista.txt_PrecioPlatillo.setEnabled(false);
        vista.cb_categoriaPlatillo.setEnabled(false);
        vista.btn_buscarImagenPlatillo.setEnabled(false);
    }
    
    public void desbloquearTxt(){
        vista.txt_nombrePlatillo.setEnabled(true);
        vista.txt_PrecioPlatillo.setEnabled(true);
        vista.cb_categoriaPlatillo.setEnabled(true);
        vista.btn_buscarImagenPlatillo.setEnabled(true);
    }
    
    public void predeterminado(){
        limpiarTxt();
        bloquearTxt();
        vista.btn_NuevoPlatillo.setEnabled(true);
        vista.btn_GuardarPlatillo.setEnabled(false);
        vista.btn_ModificarPlatillo.setEnabled(false);
        vista.btn_actualizarPlatillo.setEnabled(false);
        vista.btn_CancelarPlatillo.setEnabled(false);
    }
    
    public void btnNuevoPlatillo(){
        desbloquearTxt();
        vista.btn_NuevoPlatillo.setEnabled(false);
        vista.btn_GuardarPlatillo.setEnabled(true);
        vista.btn_ModificarPlatillo.setEnabled(false);
        vista.btn_actualizarPlatillo.setEnabled(false);
        vista.btn_CancelarPlatillo.setEnabled(true);
    }

    public void btnActualizarPlatillo(){
        desbloquearTxt();
        vista.btn_NuevoPlatillo.setEnabled(false);
        vista.btn_GuardarPlatillo.setEnabled(false);
        vista.btn_ModificarPlatillo.setEnabled(false);
        vista.btn_actualizarPlatillo.setEnabled(true);
        vista.btn_CancelarPlatillo.setEnabled(true);
    }
    
    public Boolean validaciones (String nombre, String precio){
        Pattern NOMBRE_REGEX = Pattern.compile("^([A-Za-zñáéíóúÑÁÉÍÓÚ]+\\s?)*$",Pattern.CASE_INSENSITIVE);
        Pattern PRECIO_REGEX = Pattern.compile("^\\d*.?\\d*$");
        
        Matcher matcher_nombre = NOMBRE_REGEX.matcher(nombre);
        Matcher matcher_precio = PRECIO_REGEX.matcher(precio);
        
        if(matcher_nombre.find() == true){
            if(matcher_precio.find() == true){
                return true;
            }else{
                vista.txt_PrecioPlatillo.setText("");
                JOptionPane.showMessageDialog(null, "Formato del precio incorrecto.");
                return false;
            }
        }else{
            vista.txt_nombrePlatillo.setText("");
            JOptionPane.showMessageDialog(null, "Formato del nombre incorrecto.");
            return false;
        }
    }
    
    public void GuardarImagen(){
        String ruta;
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("PNG","JPG","png","jpg");
        JFileChooser imagen = new JFileChooser();
        imagen.setFileFilter(filtro);
        imagen.setMultiSelectionEnabled(false);
        int opcion = imagen.showOpenDialog(vista);
        if(opcion == JFileChooser.APPROVE_OPTION){
            ruta = imagen.getSelectedFile().getAbsolutePath();
            Image preview = Toolkit.getDefaultToolkit().getImage(ruta);
            vista.txt_rutaImagenPlatillo.setText(ruta);
            if(preview != null){
                vista.jLabel_imagenPlatillo.setText("");
                ImageIcon icono = new ImageIcon(preview.getScaledInstance(100,100, Image.SCALE_SMOOTH));
                vista.jLabel_imagenPlatillo.setIcon(icono);
            }
        }
    }
    
    public void tablaPlatillo(){
        ArrayList<Platillo> platillos = platilloDAO.ListaPlatillo(vista.txt_buscarPlatillo.getText());
        String[] colNombres = {"id","Nombre","Precio", "Categoria", "Imagen", "Usuario id"};
        Object[][] filas = new Object[platillos.size()][8];
        for(int i = 0; i < platillos.size(); i++ ){
            filas[i][0] = platillos.get(i).getId();
            filas[i][1] = platillos.get(i).getNombre();
            filas[i][2] = platillos.get(i).getPrecio();
            filas[i][3] = platillos.get(i).getCategoria();
            ImageIcon img = new ImageIcon(new ImageIcon(platillos.get(i).getFoto()).getImage().getScaledInstance(100,100, Image.SCALE_SMOOTH));
            filas[i][4] = img;
            filas[i][5] = platillos.get(i).getUsuario_id();
        }
        TablaPlatilloModelo tablaModelo = new TablaPlatilloModelo(filas, colNombres);
        vista.tablaPlatillos.setModel(tablaModelo);
        JTableHeader encabezado = vista.tablaPlatillos.getTableHeader();
        encabezado.setOpaque(false);
        encabezado.setBackground(new Color(240,240,240));
        encabezado.setForeground(Color.black);
        vista.tablaPlatillos.setRowHeight(vista.jLabel_imagenPlatillo.getHeight()); 
    }
    
    @Override
    public void actionPerformed(ActionEvent evento) {
        if(evento.getSource() == vista.btn_NuevoPlatillo){
            limpiarTxt();
            btnNuevoPlatillo();
        }
        else if(evento.getSource() == vista.btn_ModificarPlatillo){
            btnActualizarPlatillo();
        }
        else if(evento.getSource() == vista.btn_CancelarPlatillo){
            tablaPlatillo();
            predeterminado();
        }
        else if(evento.getSource() == vista.btn_buscarImagenPlatillo){
            GuardarImagen();
        }
        else if(evento.getSource() == vista.btn_GuardarPlatillo){
            if(vista.txt_nombrePlatillo.getText().equals("") || vista.txt_PrecioPlatillo.getText().equals("") || vista.cb_categoriaPlatillo.getSelectedItem().equals("-") || vista.txt_rutaImagenPlatillo.getText().equals("")){
                JOptionPane.showMessageDialog(null, "Todos los campos son obligatorios.");
                return;
            }
            if(validaciones(vista.txt_nombrePlatillo.getText(), vista.txt_PrecioPlatillo.getText())){
                try {
                    platillo = new Platillo(vista.txt_nombrePlatillo.getText(),vista.txt_PrecioPlatillo.getText(), vista.cb_categoriaPlatillo.getSelectedItem().toString() ,Files.readAllBytes(Paths.get(vista.txt_rutaImagenPlatillo.getText())), 29);
                    if(platilloDAO.RegistrarPlatillo(platillo)){
                        tablaPlatillo();
                        predeterminado();
                        JOptionPane.showMessageDialog(null, "Platillo guardado con exito.");
                    }else{
                        limpiarTxt();
                        JOptionPane.showMessageDialog(null, "Hubo un error al guardar el platillo.");
                    }
                } catch (IOException ex) {
                    Logger.getLogger(PlatilloController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        else if(evento.getSource() == vista.btn_actualizarPlatillo){
            try {
                if(vista.txt_nombrePlatillo.getText().equals("") || vista.txt_PrecioPlatillo.getText().equals("") || vista.cb_categoriaPlatillo.getSelectedItem().equals("-")){
                    JOptionPane.showMessageDialog(null, "Todos los campos son obligatorios.");
                    return;
                }
                if(validaciones(vista.txt_nombrePlatillo.getText(), vista.txt_PrecioPlatillo.getText())){
                    if(vista.txt_rutaImagenPlatillo.getText().equals("")){
                        platillo = new Platillo(Integer.valueOf(vista.txt_idPlatillo.getText()),vista.txt_nombrePlatillo.getText(),vista.txt_PrecioPlatillo.getText(), vista.cb_categoriaPlatillo.getSelectedItem().toString(), 29);
                    }else{
                        platillo = new Platillo(Integer.valueOf(vista.txt_idPlatillo.getText()),vista.txt_nombrePlatillo.getText(),vista.txt_PrecioPlatillo.getText(), vista.cb_categoriaPlatillo.getSelectedItem().toString(),Files.readAllBytes(Paths.get(vista.txt_rutaImagenPlatillo.getText())) ,29);
                    }
                    
                    if(platilloDAO.ModificarPlatillo(platillo)){
                        tablaPlatillo();
                        predeterminado();
                        JOptionPane.showMessageDialog(null, "Platillo actualizado con exito.");
                    }else{
                        limpiarTxt();
                        JOptionPane.showMessageDialog(null, "Hubo un error al actualizar el platillo.");
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(PlatilloController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if(evento.getSource() == vista.jMenu_EliminarPlatillo){
            if(vista.txt_idPlatillo.getText().equals("")){
                JOptionPane.showMessageDialog(null, "Seleccione una fila para eliminar.");
            }else{
                platillo = new Platillo(Integer.valueOf(vista.txt_idPlatillo.getText()));
                int preguntaSalir = JOptionPane.showConfirmDialog(null, "¿Estas seguro que quieres eliminar este platillo?", "Pregunta", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (preguntaSalir == 0){
                    if(platilloDAO.EliminarPlatillo(platillo)){
                        predeterminado();
                        tablaPlatillo();
                        JOptionPane.showMessageDialog(null, "Platillo eliminado con exito.");
                    } else{
                        limpiarTxt();
                        JOptionPane.showMessageDialog(null, "Hubo un error al eliminar el platillo.");
                    }
                }
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource() == vista.tablaPlatillos){
            int fila = vista.tablaPlatillos.rowAtPoint(e.getPoint());
            vista.txt_idPlatillo.setText(vista.tablaPlatillos.getValueAt(fila, 0).toString());
            vista.txt_nombrePlatillo.setText(vista.tablaPlatillos.getValueAt(fila, 1).toString());
            vista.txt_PrecioPlatillo.setText(vista.tablaPlatillos.getValueAt(fila, 2).toString());
            vista.cb_categoriaPlatillo.setSelectedItem(vista.tablaPlatillos.getValueAt(fila, 3).toString());
            vista.jLabel_imagenPlatillo.setIcon((Icon) vista.tablaPlatillos.getValueAt(fila, 4));
            bloquearTxt();
            vista.btn_NuevoPlatillo.setEnabled(true);
            vista.btn_GuardarPlatillo.setEnabled(false);
            vista.btn_ModificarPlatillo.setEnabled(true);
            vista.btn_actualizarPlatillo.setEnabled(false);
            vista.btn_CancelarPlatillo.setEnabled(true);
        }else if(e.getSource() == vista.jLabelPlatillo){
            vista.tabbed_Opciones.setSelectedIndex(2);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        }

    @Override
    public void mouseReleased(MouseEvent e) {
        }

    @Override
    public void mouseEntered(MouseEvent e) {
        }

    @Override
    public void mouseExited(MouseEvent e) {
        }
    
    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == vista.txt_buscarPlatillo){
            tablaPlatillo();
        }
    }

    
}
