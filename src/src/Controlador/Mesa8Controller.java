
package Controlador;

import Modelo.Bebida;
import Modelo.Empleado;
import Modelo.MesaDAO;
import Modelo.Pedido;
import Modelo.Platillo;
import Modelo.TablaMesaModelo;
import Vista.Mesero_vista;
import Vista.Ticket;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.table.JTableHeader;

public class Mesa8Controller implements ActionListener,MouseListener{
    
    private Platillo platillo;
    private Bebida bebida;
    private Pedido pedido;
    private final Empleado empleadoLogeado;
    private final MesaDAO mesa8DAO;
    private final Mesero_vista vista;
    private ArrayList<Pedido> pedidos = new ArrayList();
    private int cont = 0;
    
    public Mesa8Controller(Platillo platillo, Bebida bebida, Empleado empleadoLogeado, MesaDAO mesa8DAO, Mesero_vista vista) {
        this.platillo = platillo;
        this.bebida = bebida;
        this.mesa8DAO = mesa8DAO;
        this.vista = vista;
        this.empleadoLogeado = empleadoLogeado;
        this.vista.btn_IniciarOrdenMesa8.addActionListener(this);
        this.vista.btn_AgregarItemMesa8.addActionListener(this);
        this.vista.btn_CancelarMesa8.addActionListener(this);
        this.vista.btn_AgregarPlatilloMesa8.addActionListener(this);
        this.vista.btn_AgregarBebidaMesa8.addActionListener(this);
        this.vista.btn_FinalizarOrdenMesa8.addActionListener(this);
        this.vista.btn_ticketMesa8.addActionListener(this);
        this.vista.jMenu_eliminar_Mesa8.addActionListener(this);
        this.vista.tableMesa8.addMouseListener(this);
        this.vista.jLabelCerrarSesión.addMouseListener(this);
        ComboBoxPlatillos();
        ComboBoxBebidas();
        InicializarTabla();
        InicializarMesa8();
    }
    
    /*
        INICIALIZAR TABLA DEL MENU "MESA 8":
        Si hay pedidos existentes se encargará de cargarlos, de lo contrario la tabla empezara vacia
    */
    
    public void InicializarTabla(){
        pedidos =  mesa8DAO.ObtenerPedidos(8);
        ActualizarTabla();
        vista.txt_TotalMesa8.setText(SumaTotal(true));
    }
    
    //INICIALIZAR MENU "MESA 8":
    
    public void InicializarMesa8(){
        if(pedidos.size() > 0){
            IniciarOrden();
            vista.Mesa8.setBackground(Color.red);
        }else{
            vista.Mesa8.setBackground(Color.green);
        }
    }
    
    /*
        INICIALIZAR COMBO BOX DE "PLATILLOS" Y "BEBIDAS"
        Se encargar de cargar todos los platillos y bebidas que el administrados ingreso a la base de datos.
    */
    
    public void ComboBoxPlatillos(){
        ArrayList<Platillo> platillos = mesa8DAO.PlatilloComboBox();
        for(int i = 0; i < platillos.size(); i++){
            vista.cb_platilloMesa8.addItem(platillos.get(i).getNombre());
        }
    }
    
    public void ComboBoxBebidas(){
        ArrayList<Bebida> bebidas = mesa8DAO.BebidaComboBox();
        for(int i = 0; i < bebidas.size(); i++){
            vista.cb_bebidaMesa8.addItem(bebidas.get(i).getNombre());
        }
    }
    
    //LIMPIAR TXT Y COMBO BOX DEL MENU "MESA 8"
    
    public void Limpiar(){
        vista.cb_bebidaMesa8.setSelectedIndex(0);
        vista.txt_bebidaCantidadMesa8.setText("");
        vista.cb_platilloMesa8.setSelectedIndex(0);
        vista.txt_platilloCantidadMesa8.setText("");
    }
    
    //BLOQUEAR TXT Y COMBO BOX DEL MENU "MESA 8"
    
    public void bloquear(){
        vista.cb_bebidaMesa8.setEnabled(false);
        vista.txt_bebidaCantidadMesa8.setEnabled(false);
        vista.btn_AgregarBebidaMesa8.setEnabled(false);
        vista.cb_platilloMesa8.setEnabled(false);
        vista.txt_platilloCantidadMesa8.setEnabled(false);
        vista.btn_AgregarPlatilloMesa8.setEnabled(false);
    }
    
    // FUNCIONIAMIENTO DE LOS BOTONES DEL MENU "MESA 8"
    
    public void IniciarOrden(){
        vista.btn_IniciarOrdenMesa8.setEnabled(false);
        vista.btn_AgregarItemMesa8.setEnabled(true);
        vista.btn_FinalizarOrdenMesa8.setEnabled(true);
        vista.Mesa8.setBackground(Color.red);
        vista.cb_metodoPagoMesa8.setEnabled(true);
        vista.txt_pagoClienteMesa8.setEnabled(true);
        vista.btn_ticketMesa7.setEnabled(true);
        vista.btn_ticketMesa8.setEnabled(true);
    }
    
    public void AgregarItem(){
        vista.cb_bebidaMesa8.setEnabled(true);
        vista.txt_bebidaCantidadMesa8.setEnabled(true);
        vista.btn_AgregarBebidaMesa8.setEnabled(true);
        vista.cb_platilloMesa8.setEnabled(true);
        vista.txt_platilloCantidadMesa8.setEnabled(true);
        vista.btn_AgregarPlatilloMesa8.setEnabled(true);
        
        vista.btn_AgregarItemMesa8.setEnabled(false);
        vista.btn_CancelarMesa8.setEnabled(true);
        vista.btn_FinalizarOrdenMesa8.setEnabled(false);
    }
    
    public void FinalizarOrden(){
        vista.btn_IniciarOrdenMesa8.setEnabled(true);
        vista.btn_AgregarItemMesa8.setEnabled(false);
        vista.btn_FinalizarOrdenMesa8.setEnabled(false);
        vista.btn_CancelarMesa8.setEnabled(false);
        vista.Mesa8.setBackground(Color.green);
        Limpiar();
        bloquear();
        pedidos.removeAll(pedidos);
        ActualizarTabla();
        vista.txt_TotalMesa8.setText("$0.00");
        vista.cb_metodoPagoMesa8.setSelectedIndex(0);
        vista.txt_pagoClienteMesa8.setText("");
        vista.cb_metodoPagoMesa8.setEnabled(false);
        vista.txt_pagoClienteMesa8.setEnabled(false);
        vista.btn_ticketMesa8.setEnabled(false);
    }
    
    public void ContinuarAgregandoItems(){
        vista.btn_AgregarItemMesa8.setEnabled(true);
        vista.btn_CancelarMesa8.setEnabled(false);
        vista.btn_FinalizarOrdenMesa8.setEnabled(true);
    }
    
    public void Cancelar(){
        vista.btn_AgregarItemMesa8.setEnabled(true);
        vista.btn_FinalizarOrdenMesa8.setEnabled(true);
        vista.btn_CancelarMesa8.setEnabled(false);
    }
    
    //FUNCIÓN PARA ACTUALIZAR TABLA
    
    public void ActualizarTabla(){
        String[] colNombres = {"id","item","Cantidad","Precio", "Total"};
        Object[][] filas = new Object[pedidos.size()][5];

        for(int i = 0; i < pedidos.size(); i++){
            cont = pedidos.get(i).getId() + 1;
            filas[i][0] = pedidos.get(i).getId();
            filas[i][1] = pedidos.get(i).getNombre();
            filas[i][2] = pedidos.get(i).getCantidad();
            filas[i][3] = pedidos.get(i).getPrecio();
            filas[i][4] = pedidos.get(i).getPrecio() * pedidos.get(i).getCantidad();
        }

        TablaMesaModelo tablaModelo = new TablaMesaModelo(filas, colNombres);
        vista.tableMesa8.setModel(tablaModelo);
        JTableHeader encabezado = vista.tableMesa8.getTableHeader();
        encabezado.setOpaque(false);
        encabezado.setBackground(new Color(240,240,240));
        encabezado.setForeground(Color.black);
        vista.tableMesa8.setRowHeight(30);
    }
    
    //FUNCIÓN PARA AGREGAR NUEVO PLATILLO A LA TABLA
    
    public void AgregarNuevoPlatillo(String nombre, String cantidad){
        platillo = mesa8DAO.PlatilloBuscado(nombre);
        if(platillo.getNombre()!= null){
            Pedido pedidoNuevo = new Pedido(cont,platillo.getNombre(),Double.valueOf(platillo.getPrecio()), Integer.valueOf(cantidad), Double.valueOf(platillo.getPrecio()) *Integer.valueOf(cantidad));
            cont++;
            pedidos.add(pedidoNuevo);
            ActualizarTabla();
        }
    }
    
    //FUNCIÓN PARA AGREGAR NUEVA BEBIDA A LA TABLA
    
    public void AgregarNuevaBebida(String nombre, String cantidad){
        bebida = mesa8DAO.BebidaBuscado(nombre); 
        if(bebida.getNombre()!= null){
            Pedido pedidoNuevo = new Pedido(cont,bebida.getNombre(),Double.valueOf(bebida.getPrecio()), Integer.valueOf(cantidad), Double.valueOf(bebida.getPrecio()) *Integer.valueOf(cantidad));
            cont++;
            pedidos.add(pedidoNuevo);
            ActualizarTabla();
        }
    }
    
    //FUNCIÓN PARA ELIMINAR PLATILLO O BEBIDA DE LA TABLA
    
    public void EliminarPedido(Pedido pedido){
        for(int i = 0; i < pedidos.size(); i++){
            if(pedidos.get(i).getId() == pedido.getId()){
                pedidos.remove(i);
                ActualizarTabla();
            }
        }
    }
    
    //FUNCIÓN PARA CALCULAR EL TOTAL DE LA ORDEN DE LA MESA 8
    
    public String SumaTotal(boolean ticket){
        double t = 0;
        double p = 0;
        if(vista.tableMesa8.getRowCount() > 0){
            DecimalFormat df = new DecimalFormat("#.00");
            for(int i = 0; i < vista.tableMesa8.getRowCount(); i++){
                p = Double.valueOf(vista.tableMesa8.getValueAt(i, 4).toString());
                t += p;
            }
            if(ticket){
                return "$" + String.valueOf(df.format(t));
            }else{
                return df.format(t);
            }
        }
        return "$0.00";
    }
    
    //FUNCIÓN DE VALIDACIÓN DE CANTIDADES
    
    public Boolean validacion(String cantidad){
        Pattern CANTIDAD_REGEX = Pattern.compile("^\\d+$");
         
        Matcher matcher_cantidad = CANTIDAD_REGEX.matcher(cantidad);

        if(matcher_cantidad.find() == true){
            return true;
        }else{
            vista.txt_bebidaCantidadMesa8.setText("");
            vista.txt_platilloCantidadMesa8.setText("");
            JOptionPane.showMessageDialog(null, "La cantidad debe de ser solamente un número y sin punto decimal.");
            return false;
        }
    }
        
    //FUNCIÓN DE VALIDACIÓN DE CANTIDADES DE LOS METODOS DE PAGO
    
    public Boolean validacionMetodoPago(String cantidad){
        Pattern CANTIDAD_REGEX = Pattern.compile("^\\d+$");
         
        Matcher matcher_cantidad = CANTIDAD_REGEX.matcher(cantidad);

        if(matcher_cantidad.find() == true){
            return true;
        }else{
            vista.txt_pagoClienteMesa2.setText("");
            JOptionPane.showMessageDialog(null, "Ingrese la cantidad.");
            return false;
        }
    }
    
    //INICIO DEL FUNCIONAMIENTO DEL PROGRAMA

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.btn_IniciarOrdenMesa8){
            IniciarOrden();
        } else if(e.getSource() == vista.btn_AgregarItemMesa8){
            AgregarItem();
        } else if(e.getSource() == vista.btn_CancelarMesa8){
            Limpiar();
            bloquear();
            Cancelar();
        } else if(e.getSource() == vista.btn_AgregarPlatilloMesa8){
            if(vista.txt_platilloCantidadMesa8.getText().equals("0")){
                vista.txt_platilloCantidadMesa8.setText("");
                JOptionPane.showMessageDialog(null, "La cantidad debe de ser mayor a 0.");
            }else{
                if(validacion(vista.txt_platilloCantidadMesa8.getText())){
                    AgregarNuevoPlatillo(vista.cb_platilloMesa8.getSelectedItem().toString(), vista.txt_platilloCantidadMesa8.getText());
                    vista.txt_TotalMesa8.setText(SumaTotal(true));
                    Limpiar();
                    bloquear();
                    ContinuarAgregandoItems();
                } 
            } 
        } else if(e.getSource() == vista.btn_AgregarBebidaMesa8){
            if(vista.txt_bebidaCantidadMesa8.getText().equals("0")){
                vista.txt_bebidaCantidadMesa8.setText("");
                JOptionPane.showMessageDialog(null, "La cantidad debe de ser mayor a 0.");
            }else{
                if(validacion(vista.txt_bebidaCantidadMesa8.getText())){
                    AgregarNuevaBebida(vista.cb_bebidaMesa8.getSelectedItem().toString(), vista.txt_bebidaCantidadMesa8.getText());
                    vista.txt_TotalMesa8.setText(SumaTotal(true));
                    Limpiar();
                    bloquear();
                    ContinuarAgregandoItems();
                }
            }
        } else if(e.getSource() == vista.jMenu_eliminar_Mesa8){
            if(pedido != null){
                if(mesa8DAO.EliminarPedido(pedido)){
                    EliminarPedido(pedido);
                    vista.txt_TotalMesa8.setText(SumaTotal(true));
                    pedido = null;
                }
            }else{
                JOptionPane.showMessageDialog(null, "Seleccione un item a eliminar.");
            }
        } else if(e.getSource() == vista.btn_ticketMesa8){
            Ticket t = new Ticket(0,pedidos,empleadoLogeado.getId(),0);
            t.setVisible(true);
        }else if(e.getSource() == vista.btn_FinalizarOrdenMesa8){
            if(pedidos.size() > 0){
            int preguntaSalir = JOptionPane.showConfirmDialog(null, "¿Estas seguro que quieres finalizar la orden?", "Pregunta", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (preguntaSalir == 0){
                int id_orden =  mesa8DAO.id_orden() + 1;
                if(vista.cb_metodoPagoMesa8.getSelectedItem().equals("Tarjeta")){
                    if(vista.txt_pagoClienteMesa8.getText().equals("")){
                        Ticket t = new Ticket(id_orden,pedidos,empleadoLogeado.getId(),0);
                        t.setVisible(true);
                        if(mesa8DAO.CompraFinalizada(empleadoLogeado.getId(), 8, Double.valueOf(SumaTotal(false)), vista.cb_metodoPagoMesa8.getSelectedItem().toString())){
                            if(mesa8DAO.AgregarProductosComprados(pedidos, id_orden, 8)){
                                if(mesa8DAO.EliminarPedidosAnteriores(8)){
                                   JOptionPane.showMessageDialog(null, "Compra finalizada"); 
                                    FinalizarOrden();
                                }
                            }      
                        }  
                    }else{
                        JOptionPane.showMessageDialog(null, "El pago sera con tarjeta, no necesita ingresar alguna cantidad de dinero");
                        vista.txt_pagoClienteMesa8.setText("");
                    }
                } else {
                    if(validacionMetodoPago(vista.txt_pagoClienteMesa8.getText())){
                        double pagoCliente = Double.valueOf(vista.txt_pagoClienteMesa8.getText());
                        double pagoTotal = Double.valueOf(SumaTotal(false));
                        if(pagoCliente >= pagoTotal){
                        Ticket t = new Ticket(id_orden,pedidos,empleadoLogeado.getId(),pagoCliente);
                        t.setVisible(true);
                        if(mesa8DAO.CompraFinalizada(empleadoLogeado.getId(), 8, Double.valueOf(SumaTotal(false)), vista.cb_metodoPagoMesa8.getSelectedItem().toString())){
                            if(mesa8DAO.AgregarProductosComprados(pedidos, id_orden, 8)){
                                if(mesa8DAO.EliminarPedidosAnteriores(8)){
                                   JOptionPane.showMessageDialog(null, "Compra finalizada"); 
                                    FinalizarOrden();
                                }
                            }      
                        }
                        }else{
                            JOptionPane.showMessageDialog(null, "Falta dinero para pagar la cuenta.");
                            vista.txt_pagoClienteMesa8.setText("");
                        }
                    }
                }
            }
            }else {
                JOptionPane.showMessageDialog(null, "No se han agregado platillos a la orden, orden finalizada");
                FinalizarOrden();
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource() == vista.tableMesa8){
            int fila = vista.tableMesa8.rowAtPoint(e.getPoint());
            pedido = new Pedido(Integer.valueOf(vista.tableMesa8.getValueAt(fila, 0).toString()),
                                vista.tableMesa8.getValueAt(fila, 1).toString(),
                                Double.valueOf(vista.tableMesa8.getValueAt(fila, 3).toString()),
                                Integer.valueOf(vista.tableMesa8.getValueAt(fila, 2).toString()),
                                Double.valueOf(vista.tableMesa8.getValueAt(fila, 4).toString())
            );
        }else if(e.getSource() == vista.jLabelCerrarSesión){
            mesa8DAO.GuardarPedido(pedidos,8);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
