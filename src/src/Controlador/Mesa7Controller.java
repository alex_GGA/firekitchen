
package Controlador;

import Modelo.Bebida;
import Modelo.Empleado;
import Modelo.MesaDAO;
import Modelo.Pedido;
import Modelo.Platillo;
import Modelo.TablaMesaModelo;
import Vista.Mesero_vista;
import Vista.Ticket;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.table.JTableHeader;

public class Mesa7Controller implements ActionListener, MouseListener{
    
    private Platillo platillo;
    private Bebida bebida;
    private Pedido pedido;
    private final Empleado empleadoLogeado;
    private final MesaDAO mesa7DAO;
    private final Mesero_vista vista;
    private ArrayList<Pedido> pedidos = new ArrayList();
    private int cont = 0;
    
    public Mesa7Controller(Platillo platillo, Bebida bebida, Empleado empleadoLogeado, MesaDAO mesa7DAO, Mesero_vista vista) {
        this.platillo = platillo;
        this.bebida = bebida;
        this.mesa7DAO = mesa7DAO;
        this.vista = vista;
        this.empleadoLogeado = empleadoLogeado;
        this.vista.btn_IniciarOrdenMesa7.addActionListener(this);
        this.vista.btn_AgregarItemMesa7.addActionListener(this);
        this.vista.btn_CancelarMesa7.addActionListener(this);
        this.vista.btn_AgregarPlatilloMesa7.addActionListener(this);
        this.vista.btn_AgregarBebidaMesa7.addActionListener(this);
        this.vista.btn_FinalizarOrdenMesa7.addActionListener(this);
        this.vista.btn_ticketMesa7.addActionListener(this);
        this.vista.jMenu_eliminar_Mesa7.addActionListener(this);
        this.vista.tableMesa7.addMouseListener(this);
        this.vista.jLabelCerrarSesión.addMouseListener(this);
        ComboBoxPlatillos();
        ComboBoxBebidas();
        InicializarTabla();
        InicializarMesa7();
    }
    
    /*
        INICIALIZAR TABLA DEL MENU "MESA 7":
        Si hay pedidos existentes se encargará de cargarlos, de lo contrario la tabla empezara vacia
    */
    
    public void InicializarTabla(){
        pedidos =  mesa7DAO.ObtenerPedidos(7);
        ActualizarTabla();
        vista.txt_TotalMesa7.setText(SumaTotal(true));
    }
    
    //INICIALIZAR MENU "MESA 7":
    
    public void InicializarMesa7(){
        if(pedidos.size() > 0){
            IniciarOrden();
            vista.Mesa7.setBackground(Color.red);
        }else{
            vista.Mesa7.setBackground(Color.green);
        }
    }
    
    /*
        INICIALIZAR COMBO BOX DE "PLATILLOS" Y "BEBIDAS"
        Se encargar de cargar todos los platillos y bebidas que el administrados ingreso a la base de datos.
    */
    
    public void ComboBoxPlatillos(){
        ArrayList<Platillo> platillos = mesa7DAO.PlatilloComboBox();
        for(int i = 0; i < platillos.size(); i++){
            vista.cb_platilloMesa7.addItem(platillos.get(i).getNombre());
        }
    }
    
    public void ComboBoxBebidas(){
        ArrayList<Bebida> bebidas = mesa7DAO.BebidaComboBox();
        for(int i = 0; i < bebidas.size(); i++){
            vista.cb_bebidaMesa7.addItem(bebidas.get(i).getNombre());
        }
    }
    
    //LIMPIAR TXT Y COMBO BOX DEL MENU "MESA 7"
    
    public void Limpiar(){
        vista.cb_bebidaMesa7.setSelectedIndex(0);
        vista.txt_bebidaCantidadMesa7.setText("");
        vista.cb_platilloMesa7.setSelectedIndex(0);
        vista.txt_platilloCantidadMesa7.setText("");
    }
    
    //BLOQUEAR TXT Y COMBO BOX DEL MENU "MESA 7"
    
    public void bloquear(){
        vista.cb_bebidaMesa7.setEnabled(false);
        vista.txt_bebidaCantidadMesa7.setEnabled(false);
        vista.btn_AgregarBebidaMesa7.setEnabled(false);
        vista.cb_platilloMesa7.setEnabled(false);
        vista.txt_platilloCantidadMesa7.setEnabled(false);
        vista.btn_AgregarPlatilloMesa7.setEnabled(false);
    }
    
    // FUNCIONIAMIENTO DE LOS BOTONES DEL MENU "MESA 7"
    
    public void IniciarOrden(){
        vista.btn_IniciarOrdenMesa7.setEnabled(false);
        vista.btn_AgregarItemMesa7.setEnabled(true);
        vista.btn_FinalizarOrdenMesa7.setEnabled(true);
        vista.Mesa7.setBackground(Color.red);
        vista.cb_metodoPagoMesa7.setEnabled(true);
        vista.txt_pagoClienteMesa7.setEnabled(true);
        vista.btn_ticketMesa7.setEnabled(true);
    }
    
    public void AgregarItem(){
        vista.cb_bebidaMesa7.setEnabled(true);
        vista.txt_bebidaCantidadMesa7.setEnabled(true);
        vista.btn_AgregarBebidaMesa7.setEnabled(true);
        vista.cb_platilloMesa7.setEnabled(true);
        vista.txt_platilloCantidadMesa7.setEnabled(true);
        vista.btn_AgregarPlatilloMesa7.setEnabled(true);
        
        vista.btn_AgregarItemMesa7.setEnabled(false);
        vista.btn_CancelarMesa7.setEnabled(true);
        vista.btn_FinalizarOrdenMesa7.setEnabled(false);
    }
    
    public void FinalizarOrden(){
        vista.btn_IniciarOrdenMesa7.setEnabled(true);
        vista.btn_AgregarItemMesa7.setEnabled(false);
        vista.btn_FinalizarOrdenMesa7.setEnabled(false);
        vista.btn_CancelarMesa7.setEnabled(false);
        vista.Mesa7.setBackground(Color.green);
        Limpiar();
        bloquear();
        pedidos.removeAll(pedidos);
        ActualizarTabla();
        vista.txt_TotalMesa7.setText("$0.00");
        vista.cb_metodoPagoMesa7.setSelectedIndex(0);
        vista.txt_pagoClienteMesa7.setText("");
        vista.cb_metodoPagoMesa7.setEnabled(false);
        vista.txt_pagoClienteMesa7.setEnabled(false);
        vista.btn_ticketMesa7.setEnabled(false);
    }
    
    public void ContinuarAgregandoItems(){
        vista.btn_AgregarItemMesa7.setEnabled(true);
        vista.btn_CancelarMesa7.setEnabled(false);
        vista.btn_FinalizarOrdenMesa7.setEnabled(true);
    }
    
    public void Cancelar(){
        vista.btn_AgregarItemMesa7.setEnabled(true);
        vista.btn_FinalizarOrdenMesa7.setEnabled(true);
        vista.btn_CancelarMesa7.setEnabled(false);
    }
    
    //FUNCIÓN PARA ACTUALIZAR TABLA
    
    public void ActualizarTabla(){
        String[] colNombres = {"id","item","Cantidad","Precio", "Total"};
        Object[][] filas = new Object[pedidos.size()][5];

        for(int i = 0; i < pedidos.size(); i++){
            cont = pedidos.get(i).getId() + 1;
            filas[i][0] = pedidos.get(i).getId();
            filas[i][1] = pedidos.get(i).getNombre();
            filas[i][2] = pedidos.get(i).getCantidad();
            filas[i][3] = pedidos.get(i).getPrecio();
            filas[i][4] = pedidos.get(i).getPrecio() * pedidos.get(i).getCantidad();
        }

        TablaMesaModelo tablaModelo = new TablaMesaModelo(filas, colNombres);
        vista.tableMesa7.setModel(tablaModelo);
        JTableHeader encabezado = vista.tableMesa7.getTableHeader();
        encabezado.setOpaque(false);
        encabezado.setBackground(new Color(240,240,240));
        encabezado.setForeground(Color.black);
        vista.tableMesa7.setRowHeight(30);
    }
    
    //FUNCIÓN PARA AGREGAR NUEVO PLATILLO A LA TABLA
    
    public void AgregarNuevoPlatillo(String nombre, String cantidad){
        platillo = mesa7DAO.PlatilloBuscado(nombre);
        if(platillo.getNombre()!= null){
            Pedido pedidoNuevo = new Pedido(cont,platillo.getNombre(),Double.valueOf(platillo.getPrecio()), Integer.valueOf(cantidad), Double.valueOf(platillo.getPrecio()) *Integer.valueOf(cantidad));
            cont++;
            pedidos.add(pedidoNuevo);
            ActualizarTabla();
        }
    }
    
    //FUNCIÓN PARA AGREGAR NUEVA BEBIDA A LA TABLA
    
    public void AgregarNuevaBebida(String nombre, String cantidad){
        bebida = mesa7DAO.BebidaBuscado(nombre); 
        if(bebida.getNombre()!= null){
            Pedido pedidoNuevo = new Pedido(cont,bebida.getNombre(),Double.valueOf(bebida.getPrecio()), Integer.valueOf(cantidad), Double.valueOf(bebida.getPrecio()) *Integer.valueOf(cantidad));
            cont++;
            pedidos.add(pedidoNuevo);
            ActualizarTabla();
        }
    }
    
    //FUNCIÓN PARA ELIMINAR PLATILLO O BEBIDA DE LA TABLA
    
    public void EliminarPedido(Pedido pedido){
        for(int i = 0; i < pedidos.size(); i++){
            if(pedidos.get(i).getId() == pedido.getId()){
                pedidos.remove(i);
                ActualizarTabla();
            }
        }
    }
    
    //FUNCIÓN PARA CALCULAR EL TOTAL DE LA ORDEN DE LA MESA 7
    
    public String SumaTotal(boolean ticket){
        double t = 0;
        double p = 0;
        if(vista.tableMesa7.getRowCount() > 0){
            DecimalFormat df = new DecimalFormat("#.00");
            for(int i = 0; i < vista.tableMesa7.getRowCount(); i++){
                p = Double.valueOf(vista.tableMesa7.getValueAt(i, 4).toString());
                t += p;
            }
            if(ticket){
                return "$" + String.valueOf(df.format(t));
            }else{
                return df.format(t);
            }
        }
        return "$0.00";
    }
    
    //FUNCIÓN DE VALIDACIÓN DE CANTIDADES DE LOS METODOS DE PAGO
    
    public Boolean validacionMetodoPago(String cantidad){
        Pattern CANTIDAD_REGEX = Pattern.compile("^\\d+$");
         
        Matcher matcher_cantidad = CANTIDAD_REGEX.matcher(cantidad);

        if(matcher_cantidad.find() == true){
            return true;
        }else{
            vista.txt_pagoClienteMesa2.setText("");
            JOptionPane.showMessageDialog(null, "Ingrese la cantidad.");
            return false;
        }
    }
    
    //FUNCIÓN DE VALIDACIÓN DE CANTIDADES
    
    public Boolean validacion(String cantidad){
        Pattern CANTIDAD_REGEX = Pattern.compile("^\\d+$");
         
        Matcher matcher_cantidad = CANTIDAD_REGEX.matcher(cantidad);

        if(matcher_cantidad.find() == true){
            return true;
        }else{
            vista.txt_bebidaCantidadMesa7.setText("");
            vista.txt_platilloCantidadMesa7.setText("");
            JOptionPane.showMessageDialog(null, "La cantidad debe de ser solamente un número y sin punto decimal.");
            return false;
        }
    }
    
    //INICIO DEL FUNCIONAMIENTO DEL PROGRAMA

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.btn_IniciarOrdenMesa7){
            IniciarOrden();
        } else if(e.getSource() == vista.btn_AgregarItemMesa7){
            AgregarItem();
        } else if(e.getSource() == vista.btn_CancelarMesa7){
            Limpiar();
            bloquear();
            Cancelar();
        } else if(e.getSource() == vista.btn_AgregarPlatilloMesa7){
            if(vista.txt_platilloCantidadMesa7.getText().equals("0")){
                vista.txt_platilloCantidadMesa7.setText("");
                JOptionPane.showMessageDialog(null, "La cantidad debe de ser mayor a 0.");
            }else{
                if(validacion(vista.txt_platilloCantidadMesa7.getText())){
                    AgregarNuevoPlatillo(vista.cb_platilloMesa7.getSelectedItem().toString(), vista.txt_platilloCantidadMesa7.getText());
                    vista.txt_TotalMesa7.setText(SumaTotal(true));
                    Limpiar();
                    bloquear();
                    ContinuarAgregandoItems();
                } 
            } 
        } else if(e.getSource() == vista.btn_AgregarBebidaMesa7){
            if(vista.txt_bebidaCantidadMesa7.getText().equals("0")){
                vista.txt_bebidaCantidadMesa7.setText("");
                JOptionPane.showMessageDialog(null, "La cantidad debe de ser mayor a 0.");
            }else{
                if(validacion(vista.txt_bebidaCantidadMesa7.getText())){
                    AgregarNuevaBebida(vista.cb_bebidaMesa7.getSelectedItem().toString(), vista.txt_bebidaCantidadMesa7.getText());
                    vista.txt_TotalMesa7.setText(SumaTotal(true));
                    Limpiar();
                    bloquear();
                    ContinuarAgregandoItems();
                }
            }
        } else if(e.getSource() == vista.jMenu_eliminar_Mesa7){
            if(pedido != null){
                if(mesa7DAO.EliminarPedido(pedido)){
                    EliminarPedido(pedido);
                    vista.txt_TotalMesa7.setText(SumaTotal(true));
                    pedido = null;
                }
            }else{
                JOptionPane.showMessageDialog(null, "Seleccione un item a eliminar.");
            }
        } else if(e.getSource() == vista.btn_ticketMesa7){
            Ticket t = new Ticket(0,pedidos,empleadoLogeado.getId(),0);
            t.setVisible(true);
        }else if(e.getSource() == vista.btn_FinalizarOrdenMesa7){
            if(pedidos.size() > 0){
            int preguntaSalir = JOptionPane.showConfirmDialog(null, "¿Estas seguro que quieres finalizar la orden?", "Pregunta", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (preguntaSalir == 0){
                int id_orden =  mesa7DAO.id_orden() + 1;
                if(vista.cb_metodoPagoMesa7.getSelectedItem().equals("Tarjeta")){
                    if(vista.txt_pagoClienteMesa7.getText().equals("")){
                        Ticket t = new Ticket(id_orden,pedidos,empleadoLogeado.getId(),0);
                        t.setVisible(true);
                        if(mesa7DAO.CompraFinalizada(empleadoLogeado.getId(), 7, Double.valueOf(SumaTotal(false)), vista.cb_metodoPagoMesa7.getSelectedItem().toString())){
                            if(mesa7DAO.AgregarProductosComprados(pedidos, id_orden, 7)){
                                if(mesa7DAO.EliminarPedidosAnteriores(7)){
                                   JOptionPane.showMessageDialog(null, "Compra finalizada"); 
                                    FinalizarOrden();
                                }
                            }      
                        }  
                    }else{
                        JOptionPane.showMessageDialog(null, "El pago sera con tarjeta, no necesita ingresar alguna cantidad de dinero");
                        vista.txt_pagoClienteMesa7.setText("");
                    }
                } else {
                    if(validacionMetodoPago(vista.txt_pagoClienteMesa7.getText())){
                        double pagoCliente = Double.valueOf(vista.txt_pagoClienteMesa7.getText());
                        double pagoTotal = Double.valueOf(SumaTotal(false));
                        if(pagoCliente >= pagoTotal){
                        Ticket t = new Ticket(id_orden,pedidos,empleadoLogeado.getId(),pagoCliente);
                        t.setVisible(true);
                        if(mesa7DAO.CompraFinalizada(empleadoLogeado.getId(), 7, Double.valueOf(SumaTotal(false)), vista.cb_metodoPagoMesa7.getSelectedItem().toString())){
                            if(mesa7DAO.AgregarProductosComprados(pedidos, id_orden, 7)){
                                if(mesa7DAO.EliminarPedidosAnteriores(7)){
                                   JOptionPane.showMessageDialog(null, "Compra finalizada"); 
                                    FinalizarOrden();
                                }
                            }      
                        }
                        }else{
                            JOptionPane.showMessageDialog(null, "Falta dinero para pagar la cuenta.");
                            vista.txt_pagoClienteMesa7.setText("");
                        }
                    }
                }
            }
            }else {
                JOptionPane.showMessageDialog(null, "No se han agregado platillos a la orden, orden finalizada");
                FinalizarOrden();
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource() == vista.tableMesa7){
            int fila = vista.tableMesa7.rowAtPoint(e.getPoint());
            pedido = new Pedido(Integer.valueOf(vista.tableMesa7.getValueAt(fila, 0).toString()),
                                vista.tableMesa7.getValueAt(fila, 1).toString(),
                                Double.valueOf(vista.tableMesa7.getValueAt(fila, 3).toString()),
                                Integer.valueOf(vista.tableMesa7.getValueAt(fila, 2).toString()),
                                Double.valueOf(vista.tableMesa7.getValueAt(fila, 4).toString())
            );
        }else if(e.getSource() == vista.jLabelCerrarSesión){
            mesa7DAO.GuardarPedido(pedidos,7);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
