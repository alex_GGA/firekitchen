/*
    Esta clase de tipo controlador, se encargara de controlar el menu de empleados, el cual
    se encuentra dentro del menu de gerente.
*/
package Controlador;

import Modelo.Empleado;
import Modelo.EmpleadoDAO;
import Modelo.TablaEmpleadoModelo;
import Modelo.TextPrompt;
import Vista.Gerente_vista;
import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.JTableHeader;

public class EmpleadoController implements ActionListener, MouseListener, KeyListener {

    private Empleado empleado;
    private Empleado empleadoLogeado;
    private EmpleadoDAO empleadoDAO;
    private Gerente_vista vista;
    
    
    // Constructor del controlador de empleados.
    public EmpleadoController(Empleado empledoLogeado, Empleado empleado, EmpleadoDAO empleadoDAO, Gerente_vista vista) {
        this.empleado = empleado;
        this.empleadoDAO = empleadoDAO;
        this.vista = vista;
        this.empleadoLogeado = empledoLogeado;
        this.vista.setLocationRelativeTo(null);
        this.vista.setExtendedState(vista.MAXIMIZED_BOTH);
        TextPrompt placeholder = new TextPrompt("YYYY-MM-DD", vista.txt_fechaNacimiento);
        placeholder.changeAlpha(0.75f);
        this.vista.btn_NuevoEmpleado.addActionListener(this);
        this.vista.btn_GuardarEmpleado.addActionListener(this);
        this.vista.btn_ModificarEmpleado.addActionListener(this);
        this.vista.btn_Cancelar.addActionListener(this);
        this.vista.btn_buscar.addActionListener(this);
        this.vista.btn_actualizar.addActionListener(this);
        this.vista.jMenu_EliminarEmpleado.addActionListener(this);
        this.vista.tablaEmpleados.addMouseListener(this);
        this.vista.jLabelEmpleado.addMouseListener(this);
        this.vista.txt_buscarEmpleado.addKeyListener(this);
        tablaEmpleados();
    }
    
    public void limpiarTxt(){
        vista.txt_id.setText("");
        vista.txt_nombre.setText("");
        vista.txt_Contraseña.setText("");
        vista.txt_Correo.setText("");
        vista.txt_Usuario.setText("");
        vista.txt_fechaNacimiento.setText("");
        vista.cb_Cargo.setSelectedIndex(0);
        vista.jLabel_imagen.setIcon(null);
        vista.txt_ruta.setText("");
    }

    public void bloquearTxt(){
        vista.txt_nombre.setEnabled(false);
        vista.txt_Contraseña.setEnabled(false);
        vista.txt_Correo.setEnabled(false);
        vista.txt_Usuario.setEnabled(false);
        vista.txt_fechaNacimiento.setEnabled(false);
        vista.cb_Cargo.setEnabled(false);
        vista.btn_buscar.setEnabled(false);
    }
    
    public void desbloquearTxt(){
        vista.txt_nombre.setEnabled(true);
        vista.txt_Contraseña.setEnabled(true);
        vista.txt_Correo.setEnabled(true);
        vista.txt_Usuario.setEnabled(true);
        vista.txt_fechaNacimiento.setEnabled(true);
        vista.cb_Cargo.setEnabled(true);
        vista.btn_buscar.setEnabled(true);
    }
    
    public void predeterminado(){
        limpiarTxt();
        bloquearTxt();
        vista.btn_NuevoEmpleado.setEnabled(true);
        vista.btn_GuardarEmpleado.setEnabled(false);
        vista.btn_ModificarEmpleado.setEnabled(false);
        vista.btn_actualizar.setEnabled(false);
        vista.btn_Cancelar.setEnabled(false);
    }
    
    public void btnNuevoEmpleado(){
        desbloquearTxt();
        vista.btn_NuevoEmpleado.setEnabled(false);
        vista.btn_GuardarEmpleado.setEnabled(true);
        vista.btn_ModificarEmpleado.setEnabled(false);
        vista.btn_actualizar.setEnabled(false);
        vista.btn_Cancelar.setEnabled(true);
    }
    
    public void btnActualizarEmpleado(){
        desbloquearTxt();
        vista.btn_NuevoEmpleado.setEnabled(false);
        vista.btn_GuardarEmpleado.setEnabled(false);
        vista.btn_ModificarEmpleado.setEnabled(false);
        vista.btn_actualizar.setEnabled(true);
        vista.btn_Cancelar.setEnabled(true);
    }
    
    public Boolean validaciones(String nombre, String fecha, String correo, String usuario, String password){
        Pattern NOMBRE_REGEX = Pattern.compile("^([A-ZÑÁÉÍÓÚ]{1}[a-zñáéíóú]+\\s?){3,10}$",Pattern.CASE_INSENSITIVE);
        Pattern EMAIL_REGEX = Pattern.compile("^([a-z\\d_\\.-]+)@([\\da-z\\.-]+)\\.([a-z\\.]{2,6})$", Pattern.CASE_INSENSITIVE);
        Pattern FECHA_REGEX = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}$");
        Pattern USUARIO_REGEX = Pattern.compile("^[a-z\\d_-]*$",Pattern.CASE_INSENSITIVE);
        Pattern PASSWORD_REGEX = Pattern.compile("^[a-z\\d_-]*$",Pattern.CASE_INSENSITIVE);

        Matcher matcher_nombre = NOMBRE_REGEX.matcher(nombre);
        Matcher matcher_fecha = FECHA_REGEX.matcher(fecha);
        Matcher matcher_email = EMAIL_REGEX.matcher(correo);
        Matcher matcher_usuario = USUARIO_REGEX.matcher(usuario);
        Matcher matcher_password = PASSWORD_REGEX.matcher(password);

        if(matcher_nombre.find() == true){
            if(matcher_fecha.find() == true){
                if(matcher_email.find() == true){
                    if(matcher_usuario.find() == true){
                        if(vista.txt_Usuario.getText().length() >= 3 && vista.txt_Usuario.getText().length() <= 10){
                            if(matcher_password.find() == true){
                                if(String.valueOf(vista.txt_Contraseña.getPassword()).length() >= 6 && String.valueOf(vista.txt_Contraseña.getPassword()).length() <= 18 ){
                                     return true;
                                }else{
                                    vista.txt_Contraseña.setText("");
                                    JOptionPane.showMessageDialog(null, "Formato de contraseña incorrecto: La contraseña debe de tener minimo 3 caracteres y maximo 18.");
                                    return false;
                                }
                            }else{
                                vista.txt_Contraseña.setText("");
                                JOptionPane.showMessageDialog(null, "Formato de la contraseña incorrecta: Solo se aceptan letras, numeros, guión bajo y guión medio.");
                                return false;
                            }
                        }else{
                            vista.txt_Usuario.setText("");
                            JOptionPane.showMessageDialog(null, "Formato del nombre de usuario incorrecto: El nombre de usuario debe de tener minimo 3 caracteres y maximo 10");
                            return false;
                        }
                    }else{
                        vista.txt_Usuario.setText("");
                        JOptionPane.showMessageDialog(null, "Formato del nombre de usuario incorrecto: Solo se aceptan letras, numeros, guón bajo y guón medio.");
                        return false;
                    }
                }else{  
                    vista.txt_Correo.setText("");
                    JOptionPane.showMessageDialog(null, "Formato del correo incorrecto.");
                    return false;
                }
            }else {
                vista.txt_fechaNacimiento.setText("");
                JOptionPane.showMessageDialog(null, "Formato de la fecha incorrecto.");
                return false;
            }
        }else{  
            vista.txt_nombre.setText("");
            JOptionPane.showMessageDialog(null, "Formato del nombre incorrecto");
            return false;
        }  
    }
    
    public void GuardarImagen(){
        String ruta;
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("PNG","JPG","png","jpg");
        JFileChooser imagen = new JFileChooser();
        imagen.setFileFilter(filtro);
        imagen.setMultiSelectionEnabled(false);
        int opcion = imagen.showOpenDialog(vista);
        if(opcion == JFileChooser.APPROVE_OPTION){
            ruta = imagen.getSelectedFile().getAbsolutePath();
            Image preview = Toolkit.getDefaultToolkit().getImage(ruta);
            vista.txt_ruta.setText(ruta);
            if(preview != null){
                vista.jLabel_imagen.setText("");
                ImageIcon icono = new ImageIcon(preview.getScaledInstance(100,100, Image.SCALE_SMOOTH));
                vista.jLabel_imagen.setIcon(icono);
            }
        }
    }
    
    public void tablaEmpleados(){
        ArrayList<Empleado> empleados = empleadoDAO.ListaEmpleados(vista.txt_buscarEmpleado.getText());
        String[] colNombres = {"id","Nombre","Fecha de nacimiento", "Correo", "Usuario", "Contraseña", "Cargo" ,"Foto"};
        Object[][] filas = new Object[empleados.size()][8];
        for(int i = 0; i < empleados.size(); i++ ){
            
            filas[i][0] = empleados.get(i).getId();
            filas[i][1] = empleados.get(i).getNombre();
            filas[i][2] = empleados.get(i).getFechaNacimiento();
            filas[i][3] = empleados.get(i).getEmail();
            filas[i][4] = empleados.get(i).getUsuario();
            filas[i][5] = empleados.get(i).getPassword();
            filas[i][6] = empleados.get(i).getRol();
            ImageIcon img = new ImageIcon(new ImageIcon(empleados.get(i).getFoto()).getImage().getScaledInstance(100,100, Image.SCALE_SMOOTH));
            filas[i][7] = img;
        }
        TablaEmpleadoModelo tablaModelo = new TablaEmpleadoModelo(filas, colNombres);
        vista.tablaEmpleados.setModel(tablaModelo);
        JTableHeader encabezado = vista.tablaEmpleados.getTableHeader();
        encabezado.setOpaque(false);
        encabezado.setBackground(new Color(240,240,240));
        encabezado.setForeground(Color.black);
        vista.tablaEmpleados.setRowHeight(vista.jLabel_imagen.getHeight());
        
        
        
    }
    
    @Override
    public void actionPerformed(ActionEvent evento) {
        
        if(evento.getSource() == vista.btn_NuevoEmpleado){
            limpiarTxt();
            btnNuevoEmpleado();
        }
        else if(evento.getSource() == vista.btn_ModificarEmpleado){
            if(vista.txt_id.getText().equals(String.valueOf(empleadoLogeado.getId()))){
                JOptionPane.showMessageDialog(null, "No puedes editar a este empleado.");
            }else{
                btnActualizarEmpleado();
            }
        }
        else if(evento.getSource() == vista.btn_Cancelar){
            tablaEmpleados();
            predeterminado();
        }
        else if(evento.getSource() == vista.btn_buscar){
            GuardarImagen();
        }
        else if(evento.getSource() == vista.btn_actualizar){
            if(vista.txt_nombre.getText().equals("") || String.valueOf(vista.txt_Contraseña.getPassword()).equals("") || vista.txt_Correo.getText().equals("") || vista.txt_Usuario.getText().equals("") || vista.txt_fechaNacimiento.getText().equals("") || vista.cb_Cargo.getSelectedItem().equals("-") ){
                JOptionPane.showMessageDialog(null, "Todos los campos son obligatorios.");
                return;
            }
            if(validaciones(vista.txt_nombre.getText(), vista.txt_fechaNacimiento.getText(), vista.txt_Correo.getText(), vista.txt_Usuario.getText(), String.valueOf(vista.txt_Contraseña.getPassword()))){
                try {
                    if(vista.txt_ruta.getText().equals("")){
                        empleado = new Empleado(Integer.valueOf(vista.txt_id.getText()),vista.txt_nombre.getText(), vista.txt_fechaNacimiento.getText(),vista.txt_Correo.getText(),vista.txt_Usuario.getText(),String.valueOf(vista.txt_Contraseña.getPassword()),vista.cb_Cargo.getSelectedItem().toString());
                    }else{
                        empleado = new Empleado(Integer.valueOf(vista.txt_id.getText()),vista.txt_nombre.getText(), vista.txt_fechaNacimiento.getText(),vista.txt_Correo.getText(),vista.txt_Usuario.getText(),String.valueOf(vista.txt_Contraseña.getPassword()),vista.cb_Cargo.getSelectedItem().toString(),Files.readAllBytes(Paths.get(vista.txt_ruta.getText())));  
                    }
                    
                    if(empleadoDAO.ModificarEmpleado(empleado)){
                        predeterminado();
                        tablaEmpleados();
                        JOptionPane.showMessageDialog(null, "Empleado modificado con exito.");
                    }else{
                        limpiarTxt();
                        JOptionPane.showMessageDialog(null, "Hubo un error al modificar el empleado.");
                    }
                } catch (IOException ex) {
                    Logger.getLogger(EmpleadoController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        else if(evento.getSource() == vista.btn_GuardarEmpleado){
            if(vista.txt_nombre.getText().equals("") || String.valueOf(vista.txt_Contraseña.getPassword()).equals("") || vista.txt_Correo.getText().equals("") || vista.txt_Usuario.getText().equals("") || vista.txt_fechaNacimiento.getText().equals("") || vista.cb_Cargo.getSelectedItem().equals("-") ){
                JOptionPane.showMessageDialog(null, "Todos los campos son obligatorios.");
                return;
            }
            if(validaciones(vista.txt_nombre.getText(), vista.txt_fechaNacimiento.getText(), vista.txt_Correo.getText(), vista.txt_Usuario.getText(), String.valueOf(vista.txt_Contraseña.getPassword()))){
                try {
                    empleado = new Empleado(vista.txt_nombre.getText(),Files.readAllBytes(Paths.get(vista.txt_ruta.getText())), vista.txt_fechaNacimiento.getText(),vista.txt_Correo.getText(),vista.txt_Usuario.getText(),String.valueOf(vista.txt_Contraseña.getPassword()),vista.cb_Cargo.getSelectedItem().toString());
                    if(empleadoDAO.RegistrarEmpleado(empleado)){
                        predeterminado();
                        tablaEmpleados();
                        JOptionPane.showMessageDialog(null, "Empleado guardado con exito.");
                    }else{
                        limpiarTxt();
                        JOptionPane.showMessageDialog(null, "Hubo un error al guardar el empleado.");
                    }
                } catch (IOException ex) {
                    Logger.getLogger(EmpleadoController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        else if(evento.getSource() == vista.jMenu_EliminarEmpleado){
            if(vista.txt_id.getText().equals("")){
                JOptionPane.showMessageDialog(null, "Seleccione una fila para eliminar.");
            }else{
                empleado = new Empleado(Integer.valueOf(vista.txt_id.getText()));
                if(empleado.getId() == empleadoLogeado.getId()){
                    JOptionPane.showMessageDialog(null, "No puedes eliminar a este empleado.");
                }else{
                    int preguntaEliminar = JOptionPane.showConfirmDialog(null, "¿Estas seguro que quieres eliminar este empleado?", "Pregunta", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (preguntaEliminar == 0){
                        if(empleadoDAO.EliminarEmpleado(empleado)){
                            predeterminado();
                            tablaEmpleados();
                            JOptionPane.showMessageDialog(null, "Empleado eliminado con exito.");
                        }else{
                            limpiarTxt();
                            JOptionPane.showMessageDialog(null, "Hubo un error al eliminar el empleado.");
                        }
                    }
                }
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource() == vista.tablaEmpleados){
            int fila = vista.tablaEmpleados.rowAtPoint(e.getPoint());
            vista.txt_id.setText(vista.tablaEmpleados.getValueAt(fila, 0).toString());
            vista.txt_nombre.setText(vista.tablaEmpleados.getValueAt(fila, 1).toString());
            vista.txt_fechaNacimiento.setText(vista.tablaEmpleados.getValueAt(fila, 2).toString());
            vista.txt_Correo.setText(vista.tablaEmpleados.getValueAt(fila, 3).toString());
            vista.txt_Usuario.setText(vista.tablaEmpleados.getValueAt(fila, 4).toString());
            vista.txt_Contraseña.setText(vista.tablaEmpleados.getValueAt(fila, 5).toString());
            vista.cb_Cargo.setSelectedItem(vista.tablaEmpleados.getValueAt(fila, 6).toString());
            vista.jLabel_imagen.setIcon((Icon) vista.tablaEmpleados.getValueAt(fila, 7));
            bloquearTxt();
            vista.btn_NuevoEmpleado.setEnabled(true);
            vista.btn_ModificarEmpleado.setEnabled(true);
            vista.btn_GuardarEmpleado.setEnabled(false);
            vista.btn_actualizar.setEnabled(false);
            vista.btn_Cancelar.setEnabled(true);
        }else if(e.getSource() == vista.jLabelEmpleado){
            vista.tabbed_Opciones.setSelectedIndex(1);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }

    @Override
    public void keyTyped(KeyEvent e) {
        }

    @Override
    public void keyPressed(KeyEvent e) {
       }

    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == vista.txt_buscarEmpleado){
            tablaEmpleados();
        }
    }
}
